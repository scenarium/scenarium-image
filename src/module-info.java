import io.scenarium.image.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

open module io.scenarium.image {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.flow;
	requires transitive io.scenarium.gui.flow;
	requires javafx.swing;
	requires vecmath;
	requires java.desktop;

	exports io.scenarium.image;
	exports io.scenarium.image.display.drawer;
	exports io.scenarium.image.display.drawer.imagedrawers;
	exports io.scenarium.image.filemanager.datastream.input;
	exports io.scenarium.image.filemanager.datastream.output;
	exports io.scenarium.image.filemanager.filerecorder;
	exports io.scenarium.image.filemanager.scenario;
	exports io.scenarium.image.filemanager;
	exports io.scenarium.image.operator.dataprocessing.image;
	exports io.scenarium.image.operator.dataprocessing.image.conversion;
	exports io.scenarium.image.operator.recorder;
	exports io.scenarium.image.operator.viewer;
	exports io.scenarium.image.struct.raster;
	exports io.scenarium.image.struct;

}
