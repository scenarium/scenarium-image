/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.filemanager.scenario;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.function.Consumer;

import javax.imageio.ImageIO;

import io.scenarium.core.filemanager.scenariomanager.TimedScenario;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.core.tools.ObservableValue;
import io.scenarium.image.filemanager.RasterInfo;
import io.scenarium.image.struct.raster.BufferedImageStrategy;
import io.scenarium.image.struct.raster.RasterStrategy;

import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;

public class Image extends TimedScenario {
	// private javafx.scene.image.Image loadingImage;

	@Override
	public boolean canCreateDefault() {
		return true;
	}

	@Override
	public synchronized boolean close() {
		super.close();
		return true;
	}

	@Override
	public long getBeginningTime() {
		return 0;
	}

	@Override
	public Class<?> getDataType() {
		return BufferedImage.class;
	}

	@Override
	public long getEndTime() {
		return 1;
	}

	@Override
	public long getNbFrame() {
		return 1;
	}

	@Override
	public String[] getReaderFormatNames() {
		// return List.of( "bmp", "gif", "jpeg", "png", "jpg" );
		return ImageIO.getReaderFormatNames();
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.TIMER_SCHEDULER;
	}

	// ByteRaster imgToRaster(javafx.scene.image.Image img, Consumer<Double> progressIndicator) {
	// int width = (int) img.getWidth();
	// int height = (int) img.getHeight();
	// int[] buff = new int[width * height];
	// img.getPixelReader().getPixels(0, 0, width, height, PixelFormat.getIntArgbPreInstance(), buff, 0, width);
	// if (progressIndicator != null)
	// progressIndicator.accept(2 / 3.0);
	// ByteRaster raster = new ByteRaster((int) img.getWidth(), (int) img.getHeight(), ByteRaster.RGB);
	// byte[] dest = raster.getData();
	// int index = 0;
	// for (int i = 0; i < buff.length; i++) {
	// int color = buff[i];
	// dest[index++] = (byte) ((color & 0xFF0000) >> 16);
	// dest[index++] = (byte) ((color & 0xFF00) >> 8);
	// dest[index++] = (byte) (color & 0xFF);
	// }
	// return raster;
	// }

	// @Override
	// public boolean isTimeRepresentation() {
	// return false;
	// }

	@Override
	public void load(File file, boolean backgroundLoading) throws IOException {
		// lock.lock();
		this.file = file;
		BufferedImageStrategy rs = new BufferedImageStrategy(false);
		if (file == null) {
			rs.setComputeElement(null);
			this.scenarioData = rs;
			// scenarioData = new GeographicCoordinate(48.79950087606, 2.12802613875);
			// scenarioData = new Curved(new double[][]{{0, 1}, {0, 1}});
			// scenarioData = new Object3D(Object3D.POINTS, new float[]{1, 2, 3}, 3);
			// scenarioData = new GeographicalLocalisedObject3D(Object3D.POINTS, new float[]{1, 2, 3}, 3, new GeographicCoordinate(48.8588589, 2.3475569), 0, 0, 0);
			// lock.unlock();
			rs.flip(); // Ajout du 19/11/18 ppour pas planter quand je charge une image mais je sais pas pk pas nécessaire avant...
			fireLoadChanged();
			return;
		}
		if (backgroundLoading && file.length() > 1E6) {
			javafx.scene.image.Image img = new javafx.scene.image.Image(file.toURI().toString(), true);
			ObservableValue<Double> sdp = new ObservableValue<>(0.0);

			Consumer<BufferedImageStrategy> setScenarioData = bi -> this.scenarioData = bi;

			img.progressProperty().addListener((a, b, c) -> {
				sdp.setValue(c.doubleValue() / 3);
				if (c.doubleValue() == 1) {
					sdp.setValue(1 / 3.0);
					Task<Double> t = new Task<>() {
						@Override
						protected Double call() throws Exception {
							// RasterByte _raster = imgToRaster(img, p -> updateProgress(0.5, 1));
							rs.setComputeElement(SwingFXUtils.fromFXImage(img, null));
							setScenarioData.accept(rs);
							updateProgress(1, 1);
							Thread.sleep(100);
							return 1.0;
						}
					};
					t.progressProperty().addListener((x, y, z) -> {
						sdp.setValue(1 / 3.0 + z.doubleValue() * 2 / 3);
						if (z.doubleValue() == 1) {
							rs.flip(); // Ajout du 19/11/18 ppour pas planter quand je charge une image mais je sais pas pk pas nécessaire avant...
							fireLoadChanged();
						}
						// lock.unlock();
					});
					new Thread(t).start();
				}
			});
			this.progressProperty = sdp;
		} else {
			rs.setComputeElement(SwingFXUtils.fromFXImage(new javafx.scene.image.Image(file.toURI().toString()), null));
			rs.flip(); // Ajout du 19/11/18 ppour pas planter quand je charge une image mais je sais pas pk pas nécessaire avant...
			this.scenarioData = rs;
			// lock.unlock();
			fireLoadChanged();
		}
	}

	@Override
	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {
		RasterInfo.populate(info, ((RasterStrategy) this.scenarioData).getElement());
	}

	@Override
	public synchronized void process(Long timePointer) throws Exception {
		if (this.file == null)
			return;
		BufferedImage img = SwingFXUtils.fromFXImage(new javafx.scene.image.Image(this.file.toURI().toString()), null);
		if (img != null)
			((BufferedImageStrategy) this.scenarioData).setComputeElement(img);
	}

	@Override
	public void save(File file) throws IOException {
		ImageIO.write(this.scenarioData instanceof BufferedImageStrategy ? ((BufferedImageStrategy) this.scenarioData).getComputeElement() : (BufferedImage) this.scenarioData, "BMP", file);
	}
}
