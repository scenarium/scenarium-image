/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.filemanager.scenario;

import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedHashMap;
import java.util.Objects;

import io.scenarium.core.filemanager.scenariomanager.ScenarioException;
import io.scenarium.core.filemanager.scenariomanager.TimedScenario;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.image.filemanager.RasterInfo;
import io.scenarium.image.internal.Log;
import io.scenarium.image.struct.YUVColorSpace;
import io.scenarium.image.struct.raster.BufferedImageStrategy;

public class Raw extends TimedScenario {
	private static final int GRAY = 0;
	private static final int RGB = 1;
	private static final int BGR = 2;
	private static final int YCBCR = 3;
	private static final int GRAY16 = 4;
	public static final String[] EXTENSIONS = { "raw", "inf" };
	private RandomAccessFile movieFile;
	private int channelsSequence;
	private int nbFrame;
	private int rasterType;
	private int width;
	private int height;

	@Override
	public boolean canCreateDefault() {
		return false;
	}

	@Override
	public boolean close() {
		super.close();
		try {
			if (this.movieFile != null)
				this.movieFile.close();
			this.movieFile = null;
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	@Override
	public long getBeginningTime() {
		return 0;
	}

	@Override
	public Class<?> getDataType() {
		return BufferedImage.class;
	}

	// @Override
	// public long getEndTime() {
	// return (long) (getNbGap() * getPeriod());
	// }

	@Override
	public long getNbFrame() {
		if (this.nbFrame == -1 && this.file != null)
			try {
				readHeader(this.file);
			} catch (IOException | ScenarioException e) {}
		return this.nbFrame;
	}

	@Override
	public String[] getReaderFormatNames() {
		return EXTENSIONS;
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.TIMER_SCHEDULER;
	}

	// @Override
	// public boolean isTimeRepresentation() {
	// return false;
	// }

	@Override
	public void load(File file, boolean backgroundLoading) throws IOException, ScenarioException {
		if (this.movieFile == null)
			readHeader(file);
		if (this.width == 0 || this.height == 0)
			return;
		BufferedImage raster;
		if (this.rasterType == BufferedImage.TYPE_CUSTOM) {
			ColorModel cm = new ComponentColorModel(new YUVColorSpace(), new int[] { 8, 8, 8 }, false, false, Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
			raster = buildBufferedImage(cm, cm.createCompatibleWritableRaster(this.width, this.height), false);
		} else
			raster = buildBufferedImage(this.width, this.height, this.rasterType);
		if (this.rasterType == BufferedImage.TYPE_USHORT_GRAY) {
			short[] pixels = ((DataBufferUShort) raster.getRaster().getDataBuffer()).getData();
			byte[] pixelsByte = new byte[pixels.length * 2];
			this.movieFile.read(pixelsByte, 0, pixelsByte.length);
			ByteBuffer.wrap(pixelsByte).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(pixels);
		} else {
			byte[] pixels = ((DataBufferByte) raster.getRaster().getDataBuffer()).getData();
			this.movieFile.read(pixels, 0, pixels.length);
			if (this.channelsSequence == RGB)
				for (int i = 0; i < pixels.length;) {
					byte temp = pixels[i];
					pixels[i++] = pixels[++i];
					pixels[i++] = temp;
				}
		}
		this.scenarioData = new BufferedImageStrategy(false);
		((BufferedImageStrategy) this.scenarioData).setComputeElement(raster);
		fireLoadChanged();
	}

	protected BufferedImage buildBufferedImage(int width, int height, int rasterType) {
		return new BufferedImage(width, height, rasterType);
	}

	protected BufferedImage buildBufferedImage(ColorModel cm, WritableRaster raster, boolean isRasterPremultiplied) {
		return new BufferedImage(cm, raster, isRasterPremultiplied, null);
	}

	@Override
	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {
		try {
			load(this.file, false);
			RasterInfo.populate(info, ((BufferedImageStrategy) getScenarioData()).getElement());
			info.put("Number of frames", Integer.toString(this.nbFrame));
			close();
		} catch (ScenarioException e) {
			e.printStackTrace();
		}
		info.put("Data coding format", "RAW");
	}

	@Override
	public void process(Long timePointer) {
		// Log.info("Play raw from process: " + ProcessHandle.current().pid());
		if (timePointer < 0) {
			Log.error("Negative framePointer : " + timePointer);
			return;
		}
		long frame = (long) (timePointer / getPeriod());
		if (frame >= this.nbFrame)
			Log.error("error with frame");
		BufferedImage raster = ((BufferedImageStrategy) this.scenarioData).getComputeElement();
		if (raster.getWidth() != this.width || raster.getHeight() != this.height || raster.getType() != this.rasterType) {
			if (this.rasterType == BufferedImage.TYPE_CUSTOM) {
				ColorModel cm = new ComponentColorModel(new YUVColorSpace(), new int[] { 8, 8, 8 }, false, false, Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
				raster = buildBufferedImage(cm, cm.createCompatibleWritableRaster(this.width, this.height), false);
			} else
				raster = buildBufferedImage(this.width, this.height, this.rasterType);
			((BufferedImageStrategy) this.scenarioData).setComputeElement(raster);
		}
		if (this.channelsSequence == GRAY16) {
			short[] pixels = ((DataBufferUShort) raster.getRaster().getDataBuffer()).getData();
			byte[] pixelsByte = new byte[pixels.length * 2];
			if (this.movieFile == null)
				return;
			try {
				this.movieFile.seek(frame * pixelsByte.length);
				this.movieFile.read(pixelsByte, 0, pixelsByte.length);
				ByteBuffer.wrap(pixelsByte).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(pixels);
			} catch (IOException e) {
				checkException(e);
			}
		} else {
			byte[] pixels = ((DataBufferByte) raster.getRaster().getDataBuffer()).getData();
			if (this.movieFile == null)
				return;
			try {
				this.movieFile.seek(frame * pixels.length);
				int nbBytesRead = this.movieFile.read(pixels, 0, pixels.length);
				if (nbBytesRead < 0)
					throw new IllegalArgumentException("Cannot read raw file");
				if (this.channelsSequence == RGB)
					for (int i = 0; i < pixels.length;) {
						byte temp = pixels[i];
						pixels[i++] = pixels[++i];
						pixels[i++] = temp;
					}
			} catch (IOException e) {
				checkException(e);
			}
		}
	}

	private void checkException(IOException e) {
		if (!e.getMessage().equals("Stream Closed"))
			e.printStackTrace();
	}

	private void readHeader(File file) throws IOException, ScenarioException {
		if (file == null || file.toString().isEmpty())
			return;
		String filePath = file.getAbsolutePath();
		int extIndex = filePath.lastIndexOf(".");
		String ext = filePath.substring(extIndex + 1, filePath.length());
		this.movieFile = new RandomAccessFile(ext.equals("raw") ? file : new File(filePath.substring(0, extIndex).concat(".raw")), "r");
		if (!ext.equals("inf"))
			file = new File(file.getAbsolutePath().substring(0, extIndex).concat(".inf"));
		this.file = file;
		try (BufferedReader infReader = new BufferedReader(new FileReader(file))) {
			String ligne;
			ligne = infReader.readLine();
			this.nbFrame = Integer.parseInt(ligne.substring(0, ligne.indexOf(" ")).trim());
			ligne = infReader.readLine();
			this.width = Integer.parseInt(ligne.substring(0, ligne.indexOf(" ")).trim());
			ligne = infReader.readLine();
			this.height = Integer.parseInt(ligne.substring(0, ligne.indexOf(" ")).trim());
			ligne = infReader.readLine();
			String codingFormat = ligne.substring(0, ligne.indexOf(" ")).trim();
			ligne = infReader.readLine();
			String channelsSequence = ligne.substring(0, ligne.indexOf(" ")).trim();
			infReader.readLine(); // depth
			ligne = infReader.readLine();
			int imageSize = Integer.parseInt(ligne.substring(0, ligne.indexOf(" ")).trim());
			int nbFrame2 = (int) (this.movieFile.length() / imageSize);
			if (nbFrame2 != this.nbFrame)
				Log.error("Corrupt raw file: " + file + "\nThe inf file indicated: " + this.nbFrame + " frames and the raw file contain: " + nbFrame2 + " frames");
			if (!codingFormat.equals("RAW"))
				throw new ScenarioException("Scenarium only support raw files");
			this.rasterType = -1;
			if (channelsSequence.equals("GRAY")) {
				this.channelsSequence = GRAY;
				this.rasterType = BufferedImage.TYPE_BYTE_GRAY; // Raster.GRAY;
			} else if (channelsSequence.equals("RGB")) {
				this.channelsSequence = RGB;
				this.rasterType = BufferedImage.TYPE_3BYTE_BGR; // Raster.RGB;
			} else if (channelsSequence.equals("BGR")) {
				this.channelsSequence = BGR;
				this.rasterType = BufferedImage.TYPE_3BYTE_BGR; // Raster.BGR;
			} else if (channelsSequence.equals("YCbCr")) {
				this.channelsSequence = YCBCR;
				this.rasterType = BufferedImage.TYPE_CUSTOM; // Raster.BGR;
			} else if (channelsSequence.equals("GRAY16")) {
				this.channelsSequence = GRAY16;
				this.rasterType = BufferedImage.TYPE_USHORT_GRAY; // Raster.BGR;
			} else
				throw new ScenarioException("The channelsSequence: " + channelsSequence + " is not supported");

		}
	}

	@Override
	public void save(File file) throws IOException {
		// ImageStreamRecorder isr = new ImageStreamRecorder();
		// isr.setRecordPath(file.getParent());
		// isr.pop(((BufferedImageStrategy) scenarioData).getDrawElement());
	}

	@Override
	public void setFile(File file) {
		if (Objects.equals(file, this.file))
			return;
		this.nbFrame = -1;
		super.setFile(file);
	}
}
