/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.filemanager.datastream.output;

import java.io.IOException;

import io.scenarium.image.struct.raster.ByteRaster;
import io.scenarium.river.datastream.output.GenericDataFlowOutputStream;

public class ByteRasterOutputStream extends GenericDataFlowOutputStream<ByteRaster> {

	@Override
	public void push(ByteRaster raster) throws IOException {
		this.dataOutput.writeInt(raster.getWidth());
		this.dataOutput.writeInt(raster.getHeight());
		this.dataOutput.writeInt(raster.getType());
		this.dataOutput.write(raster.getData());
	}
}
