/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.filemanager.datastream.output;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferDouble;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.io.IOException;
import java.nio.ByteBuffer;

import io.scenarium.river.datastream.output.GenericDataFlowOutputStream;

public class BufferedImageOutputStream extends GenericDataFlowOutputStream<BufferedImage> {
	private byte[] tempData;

	// public BufferedImageOutputStream(DataOutput dataOutput) {
	// this.dataOutput = dataOutput;
	// }

	@Override
	public void push(BufferedImage img) throws IOException {
		// ImageIO.write(img, "PNG", new File("patate2.png"));
		int width = img.getWidth();
		int height = img.getHeight();
		int type = img.getType();
		DataBuffer db = img.getRaster().getDataBuffer();
		if (db instanceof DataBufferByte)
			this.tempData = ((DataBufferByte) db).getData();
		else if (db instanceof DataBufferShort) {
			short[] pixels = ((DataBufferShort) db).getData();
			if (this.tempData == null || this.tempData.length != pixels.length * Short.BYTES)
				this.tempData = new byte[pixels.length * Short.BYTES];
			ByteBuffer.wrap(this.tempData).asShortBuffer().put(pixels);
		} else if (db instanceof DataBufferUShort) {
			short[] pixels = ((DataBufferUShort) db).getData();
			if (this.tempData == null || this.tempData.length != pixels.length * Short.BYTES)
				this.tempData = new byte[pixels.length * Short.BYTES];
			ByteBuffer.wrap(this.tempData).asShortBuffer().put(pixels);
		} else if (db instanceof DataBufferDouble) {
			double[] pixels = ((DataBufferDouble) db).getData();
			if (this.tempData == null || this.tempData.length != pixels.length * Double.BYTES)
				this.tempData = new byte[pixels.length * Double.BYTES];
			ByteBuffer.wrap(this.tempData).asDoubleBuffer().put(pixels);
		} else if (db instanceof DataBufferFloat) {
			float[] pixels = ((DataBufferFloat) db).getData();
			if (this.tempData == null || this.tempData.length != pixels.length * Float.BYTES)
				this.tempData = new byte[pixels.length * Float.BYTES];
			ByteBuffer.wrap(this.tempData).asFloatBuffer().put(pixels);
		} else if (db instanceof DataBufferInt) {
			int[] pixels = ((DataBufferInt) db).getData();
			if (this.tempData == null || this.tempData.length != pixels.length * Integer.BYTES)
				this.tempData = new byte[pixels.length * Integer.BYTES];
			ByteBuffer.wrap(this.tempData).asIntBuffer().put(pixels);
		} else
			throw new IllegalArgumentException("Image with an DataBuffer: " + db.getClass() + " is not supported");
		this.dataOutput.writeInt(width);
		this.dataOutput.writeInt(height);
		this.dataOutput.writeInt(type);
		this.dataOutput.write(this.tempData);
	}
}
