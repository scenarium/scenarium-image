package io.scenarium.image.filemanager.datastream.output;

import java.io.IOException;

import io.scenarium.image.struct.CompressedImage;
import io.scenarium.river.datastream.output.GenericDataFlowOutputStream;

public class CompressedImageOutputStream extends GenericDataFlowOutputStream<CompressedImage> {

	@Override
	public void push(CompressedImage value) throws IOException {
		this.dataOutput.writeShort((int) CompressedImage.serialVersionUID);
		this.dataOutput.writeUTF(value.getFormat());
		byte[] data = value.getDataRaw();
		this.dataOutput.writeInt(data.length);
		this.dataOutput.write(data);
		this.dataOutput.writeLong(value.getTimestamp());
	}
}
