package io.scenarium.image.filemanager.datastream.output;

import java.io.IOException;
import java.nio.ByteBuffer;

import io.scenarium.core.struct.Texture3D;
import io.scenarium.river.datastream.output.GenericDataFlowOutputStream;

public class Texture3DOutputStream extends GenericDataFlowOutputStream<Texture3D> {
	private final BufferedImageOutputStream bios = new BufferedImageOutputStream();

	public static byte[] floatArray2ByteArray(float[] values) {
		ByteBuffer buffer = ByteBuffer.allocate(4 * values.length);
		for (float value : values)
			buffer.putFloat(value);
		return buffer.array();
	}

	@Override
	public void push(Texture3D value) throws IOException {
		this.dataOutput.writeShort((int) Texture3D.serialVersionUID);
		byte[] data = floatArray2ByteArray(value.data);
		this.dataOutput.writeInt(data.length);
		this.dataOutput.write(data);
		this.bios.setDataOutput(this.dataOutput);
		this.bios.push(value.img);
	}
}
