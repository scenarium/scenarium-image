/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.filemanager.datastream.output;

import java.io.IOException;
import java.nio.ByteBuffer;

import io.scenarium.image.struct.raster.ShortRaster;
import io.scenarium.river.datastream.output.GenericDataFlowOutputStream;

public class ShortRasterOutputStream extends GenericDataFlowOutputStream<ShortRaster> {
	private byte[] tempData;

	@Override
	public void push(ShortRaster raster) throws IOException {
		this.dataOutput.writeInt(raster.getWidth());
		this.dataOutput.writeInt(raster.getHeight());
		this.dataOutput.writeInt(raster.getType());
		short[] pixels = raster.getData();
		if (this.tempData == null || this.tempData.length != pixels.length * Short.BYTES)
			this.tempData = new byte[pixels.length * Short.BYTES];
		ByteBuffer.wrap(this.tempData).asShortBuffer().put(pixels);
		this.dataOutput.write(this.tempData);
	}
}
