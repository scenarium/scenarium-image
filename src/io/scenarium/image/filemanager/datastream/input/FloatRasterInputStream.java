/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.filemanager.datastream.input;

import java.io.IOException;
import java.nio.ByteBuffer;

import io.scenarium.image.struct.raster.FloatRaster;
import io.scenarium.river.datastream.input.GenericDataFlowInputStream;

public class FloatRasterInputStream extends GenericDataFlowInputStream<FloatRaster> {
	private byte[] tempData;

	@Override
	public FloatRaster pop() throws IOException {
		FloatRaster rb = new FloatRaster(this.dataInput.readInt(), this.dataInput.readInt(), this.dataInput.readInt());
		if (this.tempData == null || this.tempData.length != rb.getSize() * Float.BYTES)
			this.tempData = new byte[rb.getSize() * Float.BYTES];
		this.dataInput.readFully(this.tempData);
		ByteBuffer.wrap(this.tempData).asFloatBuffer().put(rb.getData());
		return rb;
	}
}
