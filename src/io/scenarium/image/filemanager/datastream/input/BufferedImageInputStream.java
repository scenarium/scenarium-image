/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.filemanager.datastream.input;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferDouble;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;

import io.scenarium.river.datastream.input.GenericDataFlowInputStream;

public class BufferedImageInputStream extends GenericDataFlowInputStream<BufferedImage> {
	private byte[] tempData;

	@Override
	public BufferedImage pop() throws IOException {
		BufferedImage bi = buildObject(this.dataInput);
		DataBuffer db = bi.getRaster().getDataBuffer();
		if (db instanceof DataBufferByte)
			this.dataInput.readFully(((DataBufferByte) db).getData());
		else if (db instanceof DataBufferShort) {
			short[] pixels = ((DataBufferShort) db).getData();
			if (this.tempData == null || this.tempData.length != pixels.length * Short.BYTES)
				this.tempData = new byte[pixels.length * Short.BYTES];
			this.dataInput.readFully(this.tempData);
			ByteBuffer.wrap(this.tempData).asShortBuffer().get(pixels);
		} else if (db instanceof DataBufferUShort) {
			short[] pixels = ((DataBufferUShort) db).getData();
			if (this.tempData == null || this.tempData.length != pixels.length * Short.BYTES)
				this.tempData = new byte[pixels.length * Short.BYTES];
			this.dataInput.readFully(this.tempData);
			ByteBuffer.wrap(this.tempData).asShortBuffer().get(pixels);
		} else if (db instanceof DataBufferDouble) {
			double[] pixels = ((DataBufferDouble) db).getData();
			if (this.tempData == null || this.tempData.length != pixels.length * Double.BYTES)
				this.tempData = new byte[pixels.length * Double.BYTES];
			this.dataInput.readFully(this.tempData);
			ByteBuffer.wrap(this.tempData).asDoubleBuffer().get(pixels);
		} else if (db instanceof DataBufferFloat) {
			float[] pixels = ((DataBufferFloat) db).getData();
			if (this.tempData == null || this.tempData.length != pixels.length * Float.BYTES)
				this.tempData = new byte[pixels.length * Float.BYTES];
			this.dataInput.readFully(this.tempData);
			ByteBuffer.wrap(this.tempData).asFloatBuffer().get(pixels);
		} else if (db instanceof DataBufferInt) {
			int[] pixels = ((DataBufferInt) db).getData();
			if (this.tempData == null || this.tempData.length != pixels.length * Integer.BYTES)
				this.tempData = new byte[pixels.length * Integer.BYTES];
			this.dataInput.readFully(this.tempData);
			ByteBuffer.wrap(this.tempData).asIntBuffer().get(pixels);
		} else
			throw new IllegalArgumentException("Image with an DataBuffer: " + db.getClass() + " is not supported");
		return bi;
	}

	protected BufferedImage buildObject(DataInput dataInput) throws IOException {
		return new BufferedImage(dataInput.readInt(), dataInput.readInt(), dataInput.readInt());
	}

}
