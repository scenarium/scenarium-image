/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.filemanager.datastream.input;

import java.io.IOException;
import java.nio.ByteBuffer;

import io.scenarium.image.struct.raster.ShortRaster;
import io.scenarium.river.datastream.input.GenericDataFlowInputStream;

public class ShortRasterInputStream extends GenericDataFlowInputStream<ShortRaster> {
	private byte[] tempData;

	@Override
	public ShortRaster pop() throws IOException {
		ShortRaster rb = new ShortRaster(this.dataInput.readInt(), this.dataInput.readInt(), this.dataInput.readInt());
		if (this.tempData == null || this.tempData.length != rb.getSize() * Short.BYTES)
			this.tempData = new byte[rb.getSize() * Short.BYTES];
		this.dataInput.readFully(this.tempData);
		ByteBuffer.wrap(this.tempData).asShortBuffer().put(rb.getData());
		return rb;
	}

}
