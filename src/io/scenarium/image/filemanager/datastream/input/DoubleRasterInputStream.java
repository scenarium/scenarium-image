/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.filemanager.datastream.input;

import java.io.IOException;
import java.nio.ByteBuffer;

import io.scenarium.image.struct.raster.DoubleRaster;
import io.scenarium.river.datastream.input.GenericDataFlowInputStream;

public class DoubleRasterInputStream extends GenericDataFlowInputStream<DoubleRaster> {
	private byte[] tempData;

	@Override
	public DoubleRaster pop() throws IOException {
		DoubleRaster rb = new DoubleRaster(this.dataInput.readInt(), this.dataInput.readInt(), this.dataInput.readInt());
		if (this.tempData == null || this.tempData.length != rb.getSize() * Double.BYTES)
			this.tempData = new byte[rb.getSize() * Double.BYTES];
		this.dataInput.readFully(this.tempData);
		ByteBuffer.wrap(this.tempData).asDoubleBuffer().put(rb.getData());
		return rb;
	}
}
