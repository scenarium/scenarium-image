package io.scenarium.image.filemanager.datastream.input;

import java.io.IOException;

import io.scenarium.image.struct.CompressedImage;
import io.scenarium.river.datastream.input.GenericDataFlowInputStream;

public class CompressedImageInputStream extends GenericDataFlowInputStream<CompressedImage> {
	@Override
	public CompressedImage pop() throws IOException {
		long versionId = this.dataInput.readShort();
		if (versionId == 3L) {
			// Read the Version 3 of the reader.
			String format = this.dataInput.readUTF();
			int size = this.dataInput.readInt();
			byte[] data = new byte[size];
			this.dataInput.readFully(data);
			long timeStamp = this.dataInput.readLong();
			return new CompressedImage(format, data, timeStamp);
		} else if (versionId == 2L) {
			// Read the Version 2 of the reader.
			String format = this.dataInput.readUTF();
			int size = this.dataInput.readInt();
			byte[] data = new byte[size];
			this.dataInput.readFully(data);
			return new CompressedImage(format, data);
		} else if (versionId == 1L) {
			// Read the Version 1 of the reader.
			String format = this.dataInput.readUTF();
			int size = this.dataInput.readInt();
			byte[] data = new byte[size];

			this.dataInput.readFully(data);
			this.dataInput.readChar(); // remove '\n' ...
			return new CompressedImage(format, data);
		}
		throw new IOException("CompressedImage deserialisation vertion not supported: decode=" + versionId + " require<=" + CompressedImage.serialVersionUID);
	}
}
