package io.scenarium.image.filemanager.datastream.input;

import java.io.IOException;
import java.nio.ByteBuffer;

import io.scenarium.core.struct.Texture3D;
import io.scenarium.river.datastream.input.GenericDataFlowInputStream;

public class Texture3DInputStream extends GenericDataFlowInputStream<Texture3D> {
	private final BufferedImageInputStream biis = new BufferedImageInputStream();

	public static float[] byteArray2FloatArray(byte[] values) {
		ByteBuffer buffer = ByteBuffer.wrap(values);
		float[] out = new float[values.length / 4];
		for (int iii = 0; iii < out.length; iii++)
			out[iii] = buffer.getFloat();
		return out;
	}

	@Override
	public Texture3D pop() throws IOException {
		long versionId = this.dataInput.readShort();
		if (versionId == 2L) {
			// Read the Version 2 of the reader.
			int size = this.dataInput.readInt();
			byte[] data = new byte[size];
			this.dataInput.readFully(data);
			float[] textCoordinate = byteArray2FloatArray(data);
			this.biis.setDataInput(this.dataInput);
			return new Texture3D(this.biis.pop(), textCoordinate);
		}
		throw new IOException("CompressedImage deserialisation vertion not supported: decode=" + versionId + " require<=" + Texture3D.serialVersionUID);
	}

}
