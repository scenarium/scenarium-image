/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.filemanager.filerecorder;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import io.scenarium.image.operator.dataprocessing.image.conversion.TypeConverter;
import io.scenarium.image.struct.YUVColorSpace;
import io.scenarium.image.struct.raster.ByteRaster;
import io.scenarium.image.struct.raster.Raster;
import io.scenarium.image.struct.raster.ShortRaster;
import io.scenarium.river.filerecorder.recorder.GenericFileStreamRecorder;

public class RawStreamRecorder extends GenericFileStreamRecorder {
	private FileOutputStream fos;
	private String rawPath;
	private int width;
	private int height;
	private String stringType;
	private int size;
	private int depth;

	public RawStreamRecorder() {
		super("inf");
	}

	@Override
	public void close() throws IOException {
		if (this.fos != null) {
			this.fos.flush();
			this.fos.close();
			this.fos = null;
			writeHeader(false);
		}
	}

	protected void initStream(int width, int height, String stringType, int size, int depth) throws IOException {
		File infFile = getFile();
		String filePath = infFile.getAbsolutePath();
		this.rawPath = filePath.substring(0, filePath.lastIndexOf(".")).concat(".raw");
		File f = new File(this.rawPath);
		f.getParentFile().mkdirs();
		this.fos = new FileOutputStream(f);
		this.width = width;
		this.height = height;
		this.stringType = stringType;
		this.size = size;
		this.depth = depth;
		writeHeader(true);
	}

	@Override
	public void push(Object object) throws IOException {
		int type = object instanceof ByteRaster ? 0
				: object instanceof BufferedImage && ((BufferedImage) object).getData().getDataBuffer() instanceof DataBufferByte ? 1 : object instanceof ShortRaster ? 2 : 3;
		if (type <= 1) {
			ByteRaster raster = getByteRaster(object, type);
			byte[] pixels = raster.getData();
			if (this.fos == null)
				initStream(raster.getWidth(), raster.getHeight(), raster.getStringType(), pixels.length, 8);
			else if (raster.getWidth() != this.width || raster.getHeight() != this.height || !raster.getStringType().equals(this.stringType) || pixels.length != this.size)
				throw new IllegalArgumentException("Stream error: Raster property change");
			this.fos.write(pixels, 0, pixels.length);
		} else {
			ShortRaster raster = getShortRaster(object, type);
			short[] pixels = raster.getData();
			if (this.fos == null)
				initStream(raster.getWidth(), raster.getHeight(), raster.getStringType(), pixels.length * 2, 8 * 2);
			else if (raster.getWidth() != this.width || raster.getHeight() != this.height || !raster.getStringType().equals(this.stringType) || pixels.length * 2 != this.size)
				throw new IllegalArgumentException("Stream error: Raster property change");
			byte[] bytes = new byte[pixels.length * 2];
			ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(pixels);
			this.fos.write(bytes, 0, bytes.length);
		}
		this.position++;
	}

	private void writeHeader(boolean init) throws IOException {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(getFile()))) {
			bw.write((init ? "-1" : this.position) + " // number of frames");
			bw.newLine();
			bw.write(this.width + " // horizontal frame size");
			bw.newLine();
			bw.write(this.height + " // vertical frame size");
			bw.newLine();
			bw.write("RAW" + " // Data coding format");
			bw.newLine();
			bw.write(this.stringType + " // Channels sequence");
			bw.newLine();
			bw.write(this.depth + " // depth");
			bw.newLine();
			bw.write(this.size + " // imageSize");
		}
	}

	public static ByteRaster getByteRaster(Object object, int type) {
		if (type == 0)
			return (ByteRaster) object;
		BufferedImage img = (BufferedImage) object;
		DataBuffer db = img.getRaster().getDataBuffer();
		if (!(db instanceof DataBufferByte)) {
			img = new TypeConverter().process(img);
			db = img.getRaster().getDataBuffer();
		}
		byte[] pixels = ((DataBufferByte) db).getData();
		int imgType = img.getType();
		int imgRasterType = imgType == BufferedImage.TYPE_BYTE_GRAY ? Raster.GRAY : imgType == BufferedImage.TYPE_3BYTE_BGR ? Raster.BGR : -1;
		if (imgRasterType == -1 && imgType == BufferedImage.TYPE_CUSTOM && img.getColorModel().getColorSpace() instanceof YUVColorSpace)
			imgRasterType = Raster.YUV;
		return new ByteRaster(img.getWidth(), img.getHeight(), imgRasterType, pixels);
	}

	public static ShortRaster getShortRaster(Object object, int type) {
		if (type == 2)
			return (ShortRaster) object;
		BufferedImage img = (BufferedImage) object;
		DataBufferUShort db = (DataBufferUShort) img.getRaster().getDataBuffer();
		short[] pixels = db.getData();
		return new ShortRaster(img.getWidth(), img.getHeight(), Raster.GRAY, pixels);
	}
}
