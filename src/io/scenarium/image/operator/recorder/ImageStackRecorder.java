/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.recorder;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

import io.beanmanager.editors.DynamicPossibilities;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.scenarium.flow.AbstractRecorder;
import io.scenarium.image.internal.Log;

public class ImageStackRecorder extends AbstractRecorder {
	@PropertyInfo(index = 10, info = "Format of images")
	@DynamicPossibilities(possibleChoicesMethod = "getReaderFormatNames")
	private String formatName;
	@PropertyInfo(index = 11, info = "Number of leading zeros")
	@NumberInfo(min = 1)
	private int leadingZeros = 1;
	private boolean useTimeStampInName = true;
	private int imgCpt;

	@Override
	public void birth() {
		this.imgCpt = 0;
	}

	@Override
	public void death() {}

	public String getFormatName() {
		return this.formatName;
	}

	public int getLeadingZeros() {
		return this.leadingZeros;
	}

	public String[] getReaderFormatNames() {
		// return new String[] { "bmp", "gif", "jpeg", "png", "jpg"};
		return Arrays.asList(ImageIO.getReaderFormatNames()).stream().map(name -> name.toLowerCase()).distinct().toArray(String[]::new);
	}

	public boolean isUseTimeStampInName() {
		return this.useTimeStampInName;
	}

	public void process(BufferedImage img) {
		try {
			if (isRecording() && !ImageIO.write(img, this.formatName, new File(getRecordDirectory().getAbsolutePath() + File.separator + getBlockName() + "-"
					+ String.format("%0" + this.leadingZeros + "d", this.useTimeStampInName ? getTimeStamp(0) : this.imgCpt++) + "." + this.formatName)))
				Log.error(getBlockName() + ": No writter found for format: " + this.formatName + " and image of type: " + img.getType());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}

	public void setLeadingZeros(int leadingZeros) {
		this.leadingZeros = leadingZeros;
	}

	public void setUseTimeStampInName(boolean useTimeStampInName) {
		this.useTimeStampInName = useTimeStampInName;
	}
}
