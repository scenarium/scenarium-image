/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image;

import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.awt.image.Kernel;

import io.beanmanager.editors.primitive.number.NumberInfo;
import io.scenarium.flow.ParamInfo;
import io.scenarium.image.internal.Log;

public class Gain {
	public static void main(String[] args) {
		BufferedImage img = new BufferedImage(640, 480, BufferedImage.TYPE_USHORT_GRAY);
		DataBuffer db = img.getData().getDataBuffer();
		Log.info("" + db);
	}

	@NumberInfo(min = 0)
	private double gain;

	private BufferedImage outRaster;

	public void birth() {}

	public void death() {
		this.outRaster = null;
	}

	public double getGain() {
		return this.gain;
	}

	@ParamInfo(in = "In", out = "Out")
	public BufferedImage process(BufferedImage img) {
		if (this.outRaster == null || this.outRaster.getType() != img.getType() || this.outRaster.getWidth() != img.getWidth() || this.outRaster.getHeight() != img.getHeight())
			this.outRaster = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
		DataBuffer db = img.getRaster().getDataBuffer();
		if (db instanceof DataBufferByte) {
			byte[] dataIn = ((DataBufferByte) db).getData();
			byte[] dataOut = ((DataBufferByte) this.outRaster.getRaster().getDataBuffer()).getData();
			for (int i = 0; i < dataIn.length; i++) {
				int val = (int) ((dataIn[i] & 0xFF) * this.gain);
				if (val > Byte.MAX_VALUE * 2 + 1)
					val = Byte.MAX_VALUE * 2 + 1;
				dataOut[i] = (byte) val;
			}
		} else if (db instanceof DataBufferUShort) {
			short[] dataIn = ((DataBufferUShort) db).getData();
			short[] dataOut = ((DataBufferUShort) this.outRaster.getRaster().getDataBuffer()).getData();
			for (int i = 0; i < dataIn.length; i++) {
				int val = (int) ((dataIn[i] & 0xFFFF) * this.gain);
				if (val > Short.MAX_VALUE * 2 + 1)
					val = Short.MAX_VALUE * 2 + 1;
				dataOut[i] = (short) val;
			}
		} else {
			Kernel kernel = new Kernel(1, 1, new float[] { (float) this.gain });
			ConvolveOp cop = new ConvolveOp(kernel);
			cop.filter(img, this.outRaster);
		}
		return this.outRaster;
	}

	public void setGain(double gain) {
		this.gain = gain;
	}
}
