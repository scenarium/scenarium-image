/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image;

import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.util.HashMap;

import javax.vecmath.Matrix3f;

import io.beanmanager.editors.PropertyInfo;
import io.scenarium.flow.EvolvedOperator;

public class Convolution extends EvolvedOperator {
	@PropertyInfo(index = 0, info = "EDGE_NO_OP : Pixels at the edge of the source image are copied to the corresponding pixels in the destination without modification.\nEDGE_ZERO_FILL : Pixels at the edge of the destination image are set to zero.")
	private EdgeCondition edgeCondition = EdgeCondition.EDGE_NO_OP;
	@PropertyInfo(index = 1, nullable = false)
	private Matrix3f kernel = new Matrix3f(1f, 0f, -1f, 2f, 0f, -2f, 1f, 0f, -1f);
	private ConvolveOp convolveOp;
	private BufferedImage dst;

	@Override
	public void birth() {
		HashMap<Key, Object> init = new HashMap<>();
		// init.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		this.convolveOp = new ConvolveOp(
				new Kernel(3, 3,
						new float[] { this.kernel.m00, this.kernel.m01, this.kernel.m02, this.kernel.m10, this.kernel.m11, this.kernel.m12, this.kernel.m20, this.kernel.m21, this.kernel.m22 }),
				this.edgeCondition == EdgeCondition.EDGE_NO_OP ? ConvolveOp.EDGE_NO_OP : ConvolveOp.EDGE_ZERO_FILL, new RenderingHints(init));
	}

	@Override
	public void death() {
		this.convolveOp = null;
		this.dst = null;
	}

	public EdgeCondition getEdgeCondition() {
		return this.edgeCondition;
	}

	public Matrix3f getKernel() {
		return this.kernel;
	}

	public BufferedImage process(BufferedImage img) {
		if (this.dst == null || img.getType() != this.dst.getType() || img.getWidth() != this.dst.getWidth() || img.getHeight() != this.dst.getHeight())
			this.dst = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
		this.convolveOp.filter(img, this.dst);
		return this.dst;
	}

	public void setEdgeCondition(EdgeCondition edgeCondition) {
		this.edgeCondition = edgeCondition;
	}

	public void setKernel(Matrix3f kernel) {
		this.kernel = kernel;
		restart();
	}
}
