package io.scenarium.image.operator.dataprocessing.image;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.ArrayList;

import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.ParamInfo;

public class ImageDiffPrevious extends EvolvedOperator {
	private byte threshold = 50;
	private boolean enableRemanance = false;
	private int countRemanance = 3;
	private BufferedImage previous = null;
	private BufferedImage outRemanance;
	private List<BufferedImage> remanance = new ArrayList<>();
	@Override
	public void birth() throws Exception {
		previous = null;
		remanance = new ArrayList<>();
	}

	@Override
	public void death() throws Exception {
		
	}

	public byte getThreshold() {
		return threshold;
	}

	public void setThreshold(byte threshold) {
		this.threshold = threshold;
	}

	public boolean isEnableRemanance() {
		return enableRemanance;
	}

	public void setEnableRemanance(boolean enableRemanance) {
		this.enableRemanance = enableRemanance;
	}

	public int getCountRemanance() {
		return countRemanance;
	}

	public void setCountRemanance(int countRemanance) {
		if (countRemanance < 2) {
			this.countRemanance = 2;
		} else {
			this.countRemanance = countRemanance;
		}
	}

	@ParamInfo(in = "in", out = "out")
	public BufferedImage process(BufferedImage input) {
		BufferedImage localPrevious = previous;
		previous = input;
		if (localPrevious == null) {
			//Log.error("no previous");
			return null;
		}
		if (input.getWidth() != localPrevious.getWidth()) {
			//Log.error("Wrong width");
			return null;
		}
		if (input.getHeight() != localPrevious.getHeight()) {
			//Log.error("Wrong height");
			return null;
		}
		BufferedImage wip = new BufferedImage(localPrevious.getWidth(), localPrevious.getHeight(), ImageType.TYPE_BYTE_GRAY.getValue());
		for (int xxx=0; xxx<localPrevious.getWidth() ; xxx++) {
			for (int yyy=0; yyy<localPrevious.getHeight() ; yyy++) {
				int valuePrevious = localPrevious.getRGB(xxx, yyy);
				int valueCurrent = input.getRGB(xxx, yyy);
				int val = Math.abs(valueCurrent-valuePrevious);
				byte val2 = (byte)val; 
				if (val2 > threshold) {
					val = 0xFFFFFFFF;
				} else {
					val = 0;
				}
				wip.setRGB(xxx, yyy, val);
			}
		}
		if (enableRemanance) {
			remanance.add(0, wip);
			while (remanance.size() > countRemanance) {
				remanance.remove(countRemanance);
			}
			BufferedImage out = new BufferedImage(localPrevious.getWidth(), localPrevious.getHeight(), ImageType.TYPE_BYTE_GRAY.getValue());
			for (int xxx=0; xxx<localPrevious.getWidth() ; xxx++) {
				for (int yyy=0; yyy<localPrevious.getHeight() ; yyy++) {
					int tmp = 0;
					for (int iii=0; iii<remanance.size() ; iii++) {
						tmp += (byte)remanance.get(iii).getRGB(xxx, yyy) / (iii+1);
					}
					tmp = (byte)tmp;
					tmp = tmp*0x1000000 + tmp * 0x10000 + tmp * 0x100 + tmp;
					out.setRGB(xxx, yyy, tmp);
				}
			}
			outRemanance = out;
		}
		//Log.error("has out : " + out);
		return wip;
	}
	public BufferedImage getOutputRemanance() {
		return outRemanance;
	}
}
