/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.ParamInfo;

public class Noiser extends EvolvedOperator {
	private NoiseType noiseType = NoiseType.GAUSS;
	private float scaleOrProbability = 1; // noise scale for type in [0-3,7-8] and probability for type in [4-6]
	private int seed = 0;

	@Override
	public void birth() {}

	@Override
	public void death() throws Exception {}

	public NoiseType getNoiseType() {
		return this.noiseType;
	}

	public float getScaleOrProbability() {
		return this.scaleOrProbability;
	}

	public int getSeed() {
		return this.seed;
	}

	@ParamInfo(in = "In", out = "Out")
	public BufferedImage process(BufferedImage raster) {
		byte[] datas = ((DataBufferByte) raster.getRaster().getDataBuffer()).getData();
		int size = raster.getHeight() * raster.getWidth();
		switch (this.noiseType) {
		case GAUSS:
			for (int i = 0; i < size; i++) {
				double val = Math.sqrt(-2.0 * Math.log(Math.random())) * Math.cos(Math.random() * (2.0 * Math.PI)) * this.scaleOrProbability;
				val += datas[i] & 0xFF;
				if (val < 0x00)
					val = 0x00;
				if (val > 0xFF)
					val = 0xFF;
				datas[i] = (byte) val;
			}
			break;
		case UNIFORM:
			for (int i = 0; i < size; i++) {
				double val = this.scaleOrProbability * ((2.0 * Math.random() - 1.0) * Math.sqrt(3));
				val += datas[i] & 0xFF;
				if (val < 0x00)
					val = 0x00;
				if (val > 0xFF)
					val = 0xFF;
				datas[i] = (byte) val;
			}
			break;
		case CAUCHY:
			for (int i = 0; i < size; i++) {
				double val = this.scaleOrProbability * Math.tan((2.0 * Math.random() - 1.0) * Math.PI / 2.0);
				val += datas[i] & 0xFF;
				if (val < 0x00)
					val = 0x00;
				if (val > 0xFF)
					val = 0xFF;
				datas[i] = (byte) val;
			}
			break;
		case EXPONENTIAL:
			for (int i = 0; i < size; i++) {
				double val = -this.scaleOrProbability * Math.log(Math.random());
				val += datas[i] & 0xFF;
				if (val < 0x00)
					val = 0x00;
				if (val > 0xFF)
					val = 0xFF;
				datas[i] = (byte) val;
			}
			break;
		case SALT:
			for (int i = 0; i < size; i++)
				if (Math.random() < this.scaleOrProbability)
					datas[i] = (byte) 0xFF;
			break;
		case PEPPER:
			for (int i = 0; i < size; i++)
				if (Math.random() < this.scaleOrProbability)
					datas[i] = (byte) 0x00;
			break;
		case SALTANDPEPPER:
			for (int i = 0; i < size; i++) {
				double tmp = Math.random();
				if (tmp < this.scaleOrProbability)
					datas[i] = (byte) 0xFF;
				if (tmp < this.scaleOrProbability * 0.5)
					datas[i] = (byte) 0x00;
			}
			break;
		case LAPLACE:
			for (int i = 0; i < size; i++) {
				double val = Math.random() < 0.5 ? this.scaleOrProbability * Math.log(2.0 * Math.random()) / Math.sqrt(2)
						: -this.scaleOrProbability * Math.log(2.0 * (1.0 - Math.random())) / Math.sqrt(2);
				val += datas[i] & 0xFF;
				if (val < 0x00)
					val = 0x00;
				if (val > 0xFF)
					val = 0xFF;
				datas[i] = (byte) val;
			}
			break;
		case POISSON:
			for (int i = 0; i < size; i++) {
				int s = 0, n = 0;
				while (s <= this.scaleOrProbability) {
					s -= Math.log(Math.random());
					n++;
				}
				double val = n - 1;
				val += datas[i] & 0xFF;
				if (val < 0x00)
					val = 0x00;
				if (val > 0xFF)
					val = 0xFF;
				datas[i] = (byte) val;
			}
			break;
		default:
			break;
		}
		return raster;
	}

	public void setNoiseType(NoiseType noiseType) {
		this.noiseType = noiseType;
	}

	public void setScaleOrProbability(float scaleOrProbability) {
		this.scaleOrProbability = scaleOrProbability;
	}

	public void setSeed(int seed) {
		this.seed = seed;
	}
}
