/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.basic.PathInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.scenarium.core.editors.NotChangeableAtRuntime;
import io.scenarium.core.tools.SchedulerUtils;
import io.scenarium.flow.EvolvedOperator;

public class ImageToVideo extends EvolvedOperator {
	@PropertyInfo(index = 0, info = "Image folder. Images must have an index in their names and no other numeric characters")
	@PathInfo(directory = true)
	private File imagePath;
	@PropertyInfo(index = 3, unit = "ms")
	@NumberInfo(min = 0)
	@NotChangeableAtRuntime
	private int stepTime = 50;
	private ArrayList<Path> imgs;
	private int index;
	private ScheduledExecutorService timer;

	@Override
	public void birth() throws Exception {
		this.imgs = getImages(this.imagePath.getAbsolutePath());
		this.index = 0;
		onStart(() -> {
			this.timer = SchedulerUtils.getTimer(getBlockName() + "-Timer");
			this.timer.scheduleAtFixedRate(() -> {
				try {
					triggerOutput(ImageIO.read(ImageToVideo.this.imgs.get(ImageToVideo.this.index++).toFile()));
					// index = 1700;
					if (ImageToVideo.this.index == ImageToVideo.this.imgs.size())
						ImageToVideo.this.index = 0;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}, this.stepTime, this.stepTime, TimeUnit.MILLISECONDS);
		});
	}

	public BufferedImage process() {
		return null;
	}

	private static ArrayList<Path> getImages(String path) throws IOException {
		ArrayList<Path> list = new ArrayList<>();
		Files.newDirectoryStream(Paths.get(path)).forEach(p -> {
			try {
				getIndex(p);
				list.add(p);
			} catch (NumberFormatException e) {

			}
		});
		list.sort((a, b) -> Integer.compare(getIndex(a), getIndex(b)));
		return list;
	}

	private static int getIndex(Path a) {
		String fileName = a.getFileName().toString();
		fileName = fileName.replaceAll("[^\\d]", "");
		return Integer.parseInt(fileName);
	}

	@Override
	public void death() throws Exception {
		if (this.timer != null) {
			this.timer.shutdownNow();
			this.timer = null;
		}
		this.imgs = null;
		this.index = 0;
	}

	public File getImagePath() {
		return this.imagePath;
	}

	public void setImagePath(File imagePath) {
		this.imagePath = imagePath;
	}

	public int getStepTime() {
		return this.stepTime;
	}

	public void setStepTime(int stepTime) {
		this.stepTime = stepTime;
	}
}
