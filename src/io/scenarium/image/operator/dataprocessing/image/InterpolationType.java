/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image;

import java.awt.image.AffineTransformOp;

public enum InterpolationType {
	TYPE_BILINEAR(AffineTransformOp.TYPE_BILINEAR), TYPE_BICUBIC(AffineTransformOp.TYPE_BICUBIC), TYPE_NEAREST_NEIGHBOR(AffineTransformOp.TYPE_NEAREST_NEIGHBOR);

	private int value;

	private InterpolationType(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
