/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import io.beanmanager.editors.primitive.number.ControlType;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.scenarium.flow.ParamInfo;

public class Threshold {
	@NumberInfo(min = 0, max = 255, controlType = ControlType.SPINNER_AND_SLIDER)
	private int seuil;

	public void birth() {}

	public void death() {}

	public int getSeuil() {
		return this.seuil;
	}

	@ParamInfo(in = "In", out = "Out")
	public BufferedImage process(BufferedImage raster) {
		byte[] data = ((DataBufferByte) raster.getRaster().getDataBuffer()).getData();
		for (int i = 0; i < data.length; i++)
			if ((data[i] & 0xFF) < this.seuil)
				data[i] = 0;
		return raster;
	}

	public void setSeuil(int seuil) {
		this.seuil = seuil;
	}
}
