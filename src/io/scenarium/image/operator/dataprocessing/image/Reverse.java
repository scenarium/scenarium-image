/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import io.beanmanager.editors.PropertyInfo;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.ParamInfo;

public class Reverse extends EvolvedOperator {
	@PropertyInfo(index = 0)
	private boolean vertical = true;
	private boolean horizontal = true;
	private BufferedImage outRaster;

	public Reverse() {}

	public Reverse(boolean vertical, boolean horizontal) {
		this.vertical = vertical;
		this.horizontal = horizontal;
	}

	@Override
	public void birth() {}

	@Override
	public void death() {
		this.outRaster = null;
	}

	public boolean isHorizontal() {
		return this.horizontal;
	}

	public boolean isVertical() {
		return this.vertical;
	}

	@ParamInfo(in = "In", out = "Out")
	public BufferedImage process(BufferedImage image) {
		int width = image.getWidth();
		int height = image.getHeight();
		AffineTransform tx = AffineTransform.getScaleInstance(this.vertical ? -1 : 1, this.horizontal ? -1 : 1);
		tx.translate(this.vertical ? -image.getWidth() : 0, this.horizontal ? -image.getHeight() : 0);
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		if (this.outRaster == null || this.outRaster.getType() != image.getType() || this.outRaster.getWidth() != width || this.outRaster.getHeight() != height)
			this.outRaster = new BufferedImage(width, height, image.getType());
		op.filter(image, this.outRaster);
		return this.outRaster;
	}

	public void setHorizontal(boolean horizontal) {
		this.horizontal = horizontal;
	}

	public void setVertical(boolean vertical) {
		this.vertical = vertical;
	}
}
