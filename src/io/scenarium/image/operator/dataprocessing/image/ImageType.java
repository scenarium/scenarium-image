/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image;

import java.awt.image.BufferedImage;

public enum ImageType {
	TYPE_3BYTE_BGR(BufferedImage.TYPE_3BYTE_BGR), TYPE_4BYTE_ABGR(BufferedImage.TYPE_4BYTE_ABGR), TYPE_4BYTE_ABGR_PRE(BufferedImage.TYPE_4BYTE_ABGR_PRE), TYPE_BYTE_BINARY(
			BufferedImage.TYPE_BYTE_BINARY), TYPE_BYTE_GRAY(BufferedImage.TYPE_BYTE_GRAY), TYPE_BYTE_INDEXED(BufferedImage.TYPE_BYTE_INDEXED), TYPE_INT_ARGB(
					BufferedImage.TYPE_INT_ARGB), TYPE_INT_ARGB_PRE(BufferedImage.TYPE_INT_ARGB_PRE), TYPE_INT_BGR(BufferedImage.TYPE_INT_BGR), TYPE_INT_RGB(
							BufferedImage.TYPE_INT_RGB), TYPE_USHORT_555_RGB(
									BufferedImage.TYPE_USHORT_555_RGB), TYPE_USHORT_565_RGB(BufferedImage.TYPE_USHORT_565_RGB), TYPE_USHORT_GRAY(BufferedImage.TYPE_USHORT_GRAY);

	private int value;

	private ImageType(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
