/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.vecmath.Point2i;

import io.beanmanager.editors.PropertyInfo;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.ParamInfo;

public class Rescale extends EvolvedOperator {
	@PropertyInfo(index = 0)
	private Point2i size = new Point2i(640, 480);
	@PropertyInfo(index = 1)
	private InterpolationType interpolationType = InterpolationType.TYPE_BILINEAR;
	private BufferedImage outRaster;

	@Override
	public void birth() throws Exception {}

	@Override
	public void death() throws Exception {
		this.outRaster = null;
	}

	@ParamInfo(in = "In", out = "Out")
	public BufferedImage process(BufferedImage raster) {
		AffineTransform tx = new AffineTransform();
		tx.scale((float) this.size.x / raster.getWidth(), (float) this.size.y / raster.getHeight());
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		if (this.outRaster == null || this.outRaster.getType() != raster.getType() || this.outRaster.getWidth() != this.size.x || this.outRaster.getHeight() != this.size.y)
			this.outRaster = new BufferedImage(this.size.x, this.size.y, raster.getType());
		return op.filter(raster, this.outRaster);
	}

	public Point2i getSize() {
		return size;
	}

	public void setSize(Point2i size) throws IOException {
		if (size == null) {
			this.size = new Point2i(640, 480);
			return;
		}
		if(size.x <= 0 || size.y <= 0) {
			throw new IOException("Can not set a negative size for an image scaler " + size);
		}
		this.size = size;
	}

	public InterpolationType getInterpolationType() {
		return this.interpolationType;
	}

	public void setInterpolationType(InterpolationType interpolationType) {
		this.interpolationType = interpolationType;
	}

}