/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image;

import java.awt.image.BufferedImage;

import javax.vecmath.Point2i;

import io.beanmanager.editors.PropertyInfo;
import io.scenarium.flow.EvolvedOperator;

public class Crop extends EvolvedOperator {
	@PropertyInfo(index = 0, nullable = false)
	private Point2i position = new Point2i(0, 0);
	@PropertyInfo(index = 1, nullable = false, info = "")
	private Point2i size = new Point2i(640, 480);
	private BufferedImage dst;

	@Override
	public void birth() {}

	@Override
	public void death() {
		this.dst = null;
	}

	public Point2i getPosition() {
		return this.position;
	}

	public Point2i getSize() {
		return this.size;
	}

	public BufferedImage process(BufferedImage img) {
		if (this.dst == null || img.getType() != this.dst.getType() || this.dst.getWidth() != this.size.x || this.dst.getHeight() != this.size.y)
			this.dst = new BufferedImage(this.size.x, this.size.y, img.getType());
		this.dst.getGraphics().drawImage(img, 0, 0, this.size.x, this.size.y, this.position.x, this.position.y, this.position.x + this.size.x, this.position.y + this.size.y, null);
		return this.dst;
	}

	public void setPosition(Point2i position) {
		this.position = position;
	}

	public void setSize(Point2i size) {
		this.size = size;
	}
}
