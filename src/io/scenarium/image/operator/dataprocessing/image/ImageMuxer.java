/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image;

import java.awt.image.BufferedImage;

import javax.vecmath.Point2i;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.primitive.number.ControlType;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.scenarium.flow.EvolvedOperator;

public class ImageMuxer extends EvolvedOperator {
	@PropertyInfo(index = 0, info = "Width of the output image")
	@NumberInfo(min = 0, controlType = ControlType.SPINNER)
	private int width;
	@PropertyInfo(index = 1, info = "Height of the output image")
	@NumberInfo(min = 0, controlType = ControlType.SPINNER)
	private int height;
	@PropertyInfo(index = 2, info = "Array of coordinates of each input, must be equal to the number of input")
	private Point2i[] coord;
	private BufferedImage[] imgBuffer;

	@Override
	public void birth() throws Exception {}

	public BufferedImage process(BufferedImage... imgs) {
		if (this.imgBuffer == null || this.imgBuffer.length != imgs.length)
			this.imgBuffer = new BufferedImage[imgs.length];
		if (this.coord == null || this.coord.length != imgs.length)
			this.coord = new Point2i[imgs.length];
		for (int i = 0; i < imgs.length; i++)
			if (imgs[i] != null)
				this.imgBuffer[i] = imgs[i];
		BufferedImage img = new BufferedImage(this.width, this.height, BufferedImage.TYPE_3BYTE_BGR);
		for (int i = 0; i < this.imgBuffer.length; i++) {
			if (this.coord[i] == null)
				this.coord[i] = new Point2i();
			if (this.imgBuffer[i] != null)
				img.getGraphics().drawImage(this.imgBuffer[i], this.coord[i].x, this.coord[i].y, null);
		}
		if (imgs[0] != null)
			return img;
		return null;
	}

	@Override
	public void death() throws Exception {
		this.imgBuffer = null;
	}

	public int getWidth() {
		return this.width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Point2i[] getCoord() {
		return this.coord;
	}

	public void setCoord(Point2i[] coord) {
		this.coord = coord;
	}
}
