/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image.conversion;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import io.scenarium.flow.ParamInfo;
import io.scenarium.flow.operator.EvolvedVarArgsOperator;
import io.scenarium.image.internal.Log;
import io.scenarium.image.struct.CompressedImage;

public class ImgDecoder implements EvolvedVarArgsOperator {
	private Integer outWidth = null;
	private Integer outHeight = null;
	private Integer outDataSize = null;
	private Long outTimestamp = null;
	private Float outTimestampDelta = null;
	public void birth() {}

	public void death() {}

	@ParamInfo(in = "encodedData", out = "img")
	public BufferedImage process(Object... objs) {
		if (objs.length != 1) {
			Log.error("Can not process more than 1 output " + objs.length);
			return null;
		}
		if (objs[0] instanceof CompressedImage)
			try {
				BufferedImage out = ImageIO.read(new ByteArrayInputStream(((CompressedImage) objs[0]).getDataRaw()));
				outWidth = out.getWidth();
				outHeight = out.getHeight();
				outDataSize = ((CompressedImage) objs[0]).getDataRaw().length;
				outTimestamp = ((CompressedImage) objs[0]).getTimestamp();
				outTimestampDelta = (float)(System.currentTimeMillis()-outTimestamp) / 1000.0f;
				return out;
			} catch (IOException e) {
				e.printStackTrace();
			}
		else if (objs[0] instanceof byte[])
			try {
				BufferedImage out = ImageIO.read(new ByteArrayInputStream((byte[]) objs[0]));
				outWidth = out.getWidth();
				outHeight = out.getHeight();
				outDataSize =((byte[]) objs[0]).length;
				return out;
			} catch (IOException e) {
				e.printStackTrace();
			}
		outWidth = null;
		outHeight = null;
		outDataSize = null;
		outTimestamp = null;
		outTimestampDelta = null;
		return null;
	}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		return false;
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return CompressedImage.class.isAssignableFrom(additionalInput) || byte[].class.isAssignableFrom(additionalInput);
	}

	public Integer getOutputWidth() {
		return this.outWidth;
	}
	public Integer getOutputHeight() {
		return this.outHeight;
	}
	public Integer getOutputDataSize() {
		return this.outDataSize;
	}
	public Long getOutputTimeStamp() {
		return this.outTimestamp;
	}
	public Float getOutputTimeStampDelta() {
		return this.outTimestampDelta;
	}
}
