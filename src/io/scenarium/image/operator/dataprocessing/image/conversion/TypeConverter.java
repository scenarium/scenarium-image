/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.dataprocessing.image.conversion;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import io.scenarium.flow.ParamInfo;
import io.scenarium.image.operator.dataprocessing.image.ImageType;

public class TypeConverter {
	private ImageType imageType = ImageType.TYPE_3BYTE_BGR;
	private BufferedImage outRaster;

	public TypeConverter() {}

	public TypeConverter(ImageType imageType) {
		this.imageType = imageType;
	}

	public void birth() {}

	public void death() {
		this.outRaster = null;
	}

	public ImageType getImageType() {
		return this.imageType;
	}

	@ParamInfo(in = "In", out = "Out")
	public BufferedImage process(BufferedImage raster) {
		if (this.outRaster == null || this.outRaster.getWidth() != raster.getWidth() || this.outRaster.getHeight() != raster.getHeight())
			this.outRaster = new BufferedImage(raster.getWidth(), raster.getHeight(), this.imageType.getValue());
		Graphics g = this.outRaster.getGraphics();
		g.drawImage(raster, 0, 0, null);
		g.dispose();
		return this.outRaster;
	}

	public void setImageType(ImageType imageType) {
		this.imageType = imageType;
		this.outRaster = null;
	}
}
