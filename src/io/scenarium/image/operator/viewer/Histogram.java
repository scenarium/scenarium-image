/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.operator.viewer;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.util.Arrays;

import javax.vecmath.Point2i;

import io.beanmanager.BeanDesc;
import io.beanmanager.BeanManager;
import io.beanmanager.editors.container.BeanEditor;
import io.scenarium.core.struct.curve.Curvei;
import io.scenarium.core.struct.curve.EvolvedCurveSeries;
import io.scenarium.core.timescheduler.VisuableSchedulable;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.gui.flow.operator.viewer.Viewer;
import io.scenarium.image.operator.dataprocessing.image.ImageType;
import io.scenarium.image.operator.dataprocessing.image.conversion.TypeConverter;

public class Histogram extends EvolvedOperator implements VisuableSchedulable {
	int[][][] datas = new int[1][2][0];
	private Viewer viewer;

	@Override
	public void birth() throws Exception {
		if (this.viewer == null) {
			String beanName = BeanEditor.getBeanDesc(this).name + "-Viewer";
			BeanDesc<?> beanDesc = BeanEditor.getRegisterBean(Viewer.class, beanName);
			if (beanDesc == null) {
				this.viewer = new Viewer();
				this.viewer.setPosition(new Point2i(0, 0));
				this.viewer.setDimension(new Point2i(640, 480));
				BeanEditor.registerBean(this.viewer, BeanEditor.getBeanDesc(this).name + "-Viewer", BeanManager.defaultDir);
			} else
				this.viewer = (Viewer) beanDesc.bean;
		}
		this.viewer.birth();
		// ((ChartDrawer) viewer.getTheaterPane()).setColors(new Color[] { Color.RED, Color.LIME, Color.BLUE });//que si tp sinon null...
	}

	@Override
	public void death() throws Exception {
		this.datas = new int[1][2][0];
		if (this.viewer != null)
			this.viewer.death();
	}

	public Viewer getViewer() {
		return this.viewer;
	}

	@Override
	public boolean needToBeSaved() {
		return this.viewer == null ? true : this.viewer.needToBeSaved();
	}

	@Override
	public void paint() {
		this.viewer.paint();
	}

	public void process(BufferedImage img) {
		int size = (img.getType() == BufferedImage.TYPE_USHORT_GRAY ? Short.MAX_VALUE : Byte.MAX_VALUE) * 2 + 2;
		int nbChannel = img.getType() == BufferedImage.TYPE_USHORT_GRAY || img.getType() == BufferedImage.TYPE_BYTE_GRAY ? 1 : 3;

		if (this.datas.length != nbChannel)
			this.datas = new int[nbChannel][2][0];
		if (this.datas[0].length != size)
			for (int i = 0; i < nbChannel; i++) {
				int[][] channel = this.datas[i];
				if (channel[0].length != size) {
					channel[0] = new int[size];
					channel[1] = new int[size];
					int[] x = this.datas[i][0];
					for (int j = 0; j < x.length; j++)
						x[j] = j;
				}
			}
		for (int[][] channel : this.datas)
			Arrays.fill(channel[1], 0);

		if (img.getType() == BufferedImage.TYPE_USHORT_GRAY || img.getType() == BufferedImage.TYPE_BYTE_GRAY) {
			int[] y = this.datas[0][1];
			if (img.getType() == BufferedImage.TYPE_USHORT_GRAY)
				for (short pixel : ((DataBufferUShort) img.getRaster().getDataBuffer()).getData())
					y[Short.toUnsignedInt(pixel)]++;
			else
				for (byte pixel : ((DataBufferByte) img.getRaster().getDataBuffer()).getData())
					y[pixel & 0xFF]++;
		} else {
			if (img.getType() != BufferedImage.TYPE_3BYTE_BGR)
				img = new TypeConverter(ImageType.TYPE_3BYTE_BGR).process(img);

			byte[] pixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
			int[][] b = this.datas[0];
			int[][] g = this.datas[1];
			int[][] r = this.datas[2];
			for (int i = 0; i < pixels.length;) {
				b[1][pixels[i++] & 0xFF]++;
				g[1][pixels[i++] & 0xFF]++;
				r[1][pixels[i++] & 0xFF]++;
			}
		}
		try {
			EvolvedCurveSeries ecs = nbChannel == 1 ? new EvolvedCurveSeries(new Curvei(this.datas[0], "Gray Levels"))
					: new EvolvedCurveSeries("Histogram", "Height", "Frequency", new Curvei(this.datas[2], "Red Levels"), new Curvei(this.datas[1], "Green Levels"),
							new Curvei(this.datas[0], "Blue Levels"));
			this.viewer.process(ecs);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setAnimated(boolean animated) {
		this.viewer.setAnimated(animated);
	}

	public void setViewer(Viewer viewer) {
		if (this.viewer != null)
			BeanEditor.unregisterBean(this.viewer, true);
		this.viewer = viewer;
		if (viewer != null)
			BeanEditor.registerBean(viewer, BeanEditor.getBeanDesc(this).name + "-Viewer", BeanManager.defaultDir);
	}
}
