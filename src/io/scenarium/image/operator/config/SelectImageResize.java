package io.scenarium.image.operator.config;

import javax.vecmath.Point2i;

import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.ParamInfo;

public class SelectImageResize extends EvolvedOperator {
	private Point2i[] sizes = {
            new Point2i(32,18),
            new Point2i(256,144),
            new Point2i(426,240),
            new Point2i(640,360),
            new Point2i(800,450),
            new Point2i(960,540),
            new Point2i(1280,720),
            new Point2i(1600,900),
            new Point2i(1920,1080),
            new Point2i(2560,1440),
            new Point2i(3840,2160)
    	};
	private Integer lastLevel = null;
	private Integer lastForcing = null;
	@Override
	public void birth() throws Exception {
		// TODO add a on start event an output generation of a data for the default value ???
		
	}
	@Override
	public void death() throws Exception {
		// nothing to do ...
	}
	@ParamInfo(in = {"level", "forcing"}, out = "size")
	public Point2i process(Integer level, Integer forcing) throws Exception {
		// update input properties
		if (level != null) {
			this.lastLevel = level;
		}
		if (forcing != null) {
			if (forcing < 0) {
				this.lastForcing = null;
			} else {
				this.lastForcing = forcing;
			}
		}
		// select between the default value and the forcing
		Integer value = this.lastLevel;
		if (value == null) {
			value = 0;
		}
		if (this.lastForcing != null) {
			value = this.lastForcing;
		}
		// generate output:
		if (this.sizes == null || this.sizes.length == 0) {
			return new Point2i(10,10);
		}
		if (value < 0) {
			value = 0;
		}
		if (value > this.sizes.length) {
			value = this.sizes.length - 1;
		}
		return this.sizes[value];
	}
	
	public Point2i[] getSizes() {
		return sizes;
	}
	public void setSizes(Point2i[] sizes) {
		this.sizes = sizes;
	}
	
	
}