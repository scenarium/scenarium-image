package io.scenarium.image;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;

import io.beanmanager.PluginsBeanSupplier;
import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.scenarium.core.PluginsCoreSupplier;
import io.scenarium.core.consumer.ScenarioConsumer;
import io.scenarium.core.struct.Texture3D;
import io.scenarium.flow.consumer.ClonerConsumer;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.gui.core.PluginsGuiCoreSupplier;
import io.scenarium.gui.core.consumer.DrawersConsumer;
import io.scenarium.image.display.drawer.ImageDrawer;
import io.scenarium.image.filemanager.datastream.input.BufferedImageInputStream;
import io.scenarium.image.filemanager.datastream.input.ByteRasterInputStream;
import io.scenarium.image.filemanager.datastream.input.CompressedImageInputStream;
import io.scenarium.image.filemanager.datastream.input.DoubleRasterInputStream;
import io.scenarium.image.filemanager.datastream.input.FloatRasterInputStream;
import io.scenarium.image.filemanager.datastream.input.IntegerRasterInputStream;
import io.scenarium.image.filemanager.datastream.input.ShortRasterInputStream;
import io.scenarium.image.filemanager.datastream.input.Texture3DInputStream;
import io.scenarium.image.filemanager.datastream.output.BufferedImageOutputStream;
import io.scenarium.image.filemanager.datastream.output.ByteRasterOutputStream;
import io.scenarium.image.filemanager.datastream.output.CompressedImageOutputStream;
import io.scenarium.image.filemanager.datastream.output.DoubleRasterOutputStream;
import io.scenarium.image.filemanager.datastream.output.FloatRasterOutputStream;
import io.scenarium.image.filemanager.datastream.output.IntegerRasterOutputStream;
import io.scenarium.image.filemanager.datastream.output.ShortRasterOutputStream;
import io.scenarium.image.filemanager.datastream.output.Texture3DOutputStream;
import io.scenarium.image.filemanager.filerecorder.RawStreamRecorder;
import io.scenarium.image.filemanager.scenario.Image;
import io.scenarium.image.filemanager.scenario.Raw;
import io.scenarium.image.operator.config.SelectImageResize;
import io.scenarium.image.operator.dataprocessing.image.Convolution;
import io.scenarium.image.operator.dataprocessing.image.Crop;
import io.scenarium.image.operator.dataprocessing.image.Gain;
import io.scenarium.image.operator.dataprocessing.image.ImageDiffPrevious;
import io.scenarium.image.operator.dataprocessing.image.ImageMuxer;
import io.scenarium.image.operator.dataprocessing.image.ImageToVideo;
import io.scenarium.image.operator.dataprocessing.image.Noiser;
import io.scenarium.image.operator.dataprocessing.image.Rescale;
import io.scenarium.image.operator.dataprocessing.image.Reverse;
import io.scenarium.image.operator.dataprocessing.image.Threshold;
import io.scenarium.image.operator.dataprocessing.image.conversion.ImgDecoder;
import io.scenarium.image.operator.dataprocessing.image.conversion.ImgEncoder;
import io.scenarium.image.operator.dataprocessing.image.conversion.TypeConverter;
import io.scenarium.image.operator.recorder.ImageStackRecorder;
import io.scenarium.image.operator.viewer.Histogram;
import io.scenarium.image.struct.CompressedImage;
import io.scenarium.image.struct.raster.BufferedImageStrategy;
import io.scenarium.image.struct.raster.ByteRaster;
import io.scenarium.image.struct.raster.DoubleRaster;
import io.scenarium.image.struct.raster.FloatRaster;
import io.scenarium.image.struct.raster.IntegerRaster;
import io.scenarium.image.struct.raster.RasterStrategy;
import io.scenarium.image.struct.raster.ShortRaster;
import io.scenarium.pluginManager.PluginsSupplier;
import io.scenarium.river.PluginsStreamSupplier;
import io.scenarium.river.consumer.DataStreamConsumer;
import io.scenarium.river.consumer.FileStreamConsumer;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier, PluginsCoreSupplier, PluginsGuiCoreSupplier, PluginsBeanSupplier, PluginsStreamSupplier {
	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		// operatorConsumer.accept(NMEA0183Decoder.class);
		operatorConsumer.accept(Threshold.class);
		operatorConsumer.accept(Rescale.class);
		operatorConsumer.accept(Gain.class);
		operatorConsumer.accept(ImageMuxer.class);
		operatorConsumer.accept(Convolution.class);
		operatorConsumer.accept(Crop.class);
		operatorConsumer.accept(Histogram.class);
		operatorConsumer.accept(ImageStackRecorder.class);
		operatorConsumer.accept(ImageToVideo.class);
		operatorConsumer.accept(ImgEncoder.class);
		operatorConsumer.accept(ImgDecoder.class);
		operatorConsumer.accept(Reverse.class);
		operatorConsumer.accept(TypeConverter.class);
		operatorConsumer.accept(Noiser.class);
		operatorConsumer.accept(SelectImageResize.class);
		operatorConsumer.accept(ImageDiffPrevious.class);
	}

	@Override
	public void populateCloners(ClonerConsumer clonerConsumer) {
		clonerConsumer.accept(BufferedImage.class, o -> {
			BufferedImage bi = o;
			ColorModel cm = bi.getColorModel();
			return new BufferedImage(cm, bi.copyData(null), cm.isAlphaPremultiplied(), null);
		});
		clonerConsumer.accept(ByteRaster.class, ByteRaster::clone);
		clonerConsumer.accept(ShortRaster.class, ShortRaster::clone);
		clonerConsumer.accept(IntegerRaster.class, IntegerRaster::clone);
		clonerConsumer.accept(FloatRaster.class, FloatRaster::clone);
		clonerConsumer.accept(DoubleRaster.class, DoubleRaster::clone);
		clonerConsumer.accept(CompressedImage.class, o -> o);
	}

	@Override
	public void populateClassNameRedirection(ClassNameRedirectionConsumer classNameRedirectionConsumer) {
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.Convolution", "io.scenarium.image.operator.dataprocessing.image.Convolution");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.Crop", "io.scenarium.image.operator.dataprocessing.image.Crop");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.Gain", "io.scenarium.image.operator.dataprocessing.image.Gain");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.ImageMuxer", "io.scenarium.image.operator.dataprocessing.image.ImageMuxer");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.ImageToVideo", "io.scenarium.image.operator.dataprocessing.image.ImageToVideo");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.Noiser", "io.scenarium.image.operator.dataprocessing.image.Noiser");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.Rescale", "io.scenarium.image.operator.dataprocessing.image.Rescale");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.Reverse", "io.scenarium.image.operator.dataprocessing.image.Reverse");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.Threshold", "io.scenarium.image.operator.dataprocessing.image.Threshold");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.conversion.ImgDecoder", "io.scenarium.image.operator.dataprocessing.image.conversion.ImgDecoder");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.conversion.ImgEncoder", "io.scenarium.image.operator.dataprocessing.image.conversion.ImgEncoder");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.dataprocessing.image.conversion.TypeConverter", "io.scenarium.image.operator.dataprocessing.image.conversion.TypeConverter");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.recorder.ImageStackRecorder", "io.scenarium.image.operator.recorder.ImageStackRecorder");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.viewer.Histogram", "io.scenarium.image.operator.viewer.Histogram");
		classNameRedirectionConsumer.accept("io.scenarium.core.struct.CompressedImage", "io.scenarium.image.struct.CompressedImage");
		classNameRedirectionConsumer.accept("io.scenarium.core.display.drawer.ImageDrawer", "io.scenarium.image.display.drawer.ImageDrawer");
	}

	@Override
	public void populateDrawers(DrawersConsumer drawersConsumer) {
		drawersConsumer.accept(ByteRaster.class, ImageDrawer.class);
		drawersConsumer.accept(FloatRaster.class, ImageDrawer.class);
		drawersConsumer.accept(DoubleRaster.class, ImageDrawer.class);
		drawersConsumer.accept(ShortRaster.class, ImageDrawer.class);
		drawersConsumer.accept(IntegerRaster.class, ImageDrawer.class);
		drawersConsumer.accept(RasterStrategy.class, ImageDrawer.class);
		drawersConsumer.accept(BufferedImageStrategy.class, ImageDrawer.class);
		drawersConsumer.accept(BufferedImage.class, ImageDrawer.class);
	}

	@Override
	public void populateDataStream(DataStreamConsumer dataStreamConsumer) {
		dataStreamConsumer.accept(BufferedImage.class, BufferedImageInputStream.class, BufferedImageOutputStream.class);
		dataStreamConsumer.accept(ByteRaster.class, ByteRasterInputStream.class, ByteRasterOutputStream.class);
		dataStreamConsumer.accept(ShortRaster.class, ShortRasterInputStream.class, ShortRasterOutputStream.class);
		dataStreamConsumer.accept(IntegerRaster.class, IntegerRasterInputStream.class, IntegerRasterOutputStream.class);
		dataStreamConsumer.accept(FloatRaster.class, FloatRasterInputStream.class, FloatRasterOutputStream.class);
		dataStreamConsumer.accept(DoubleRaster.class, DoubleRasterInputStream.class, DoubleRasterOutputStream.class);
		dataStreamConsumer.accept(CompressedImage.class, CompressedImageInputStream.class, CompressedImageOutputStream.class);
		dataStreamConsumer.accept(Texture3D.class, Texture3DInputStream.class, Texture3DOutputStream.class);
	}

	@Override
	public void populateFileStream(FileStreamConsumer fileStreamConsumer) {
		fileStreamConsumer.accept(BufferedImage.class, "inf", null, RawStreamRecorder.class);
		// fileStreamConsumer.accept(BufferedImageStrategy.class, "inf", null, RawStreamRecorder.class);
	}

	@Override
	public void populateScenarios(ScenarioConsumer scenarioConsumer) {
		scenarioConsumer.accept(Image.class);
		scenarioConsumer.accept(Raw.class);
	}

}
