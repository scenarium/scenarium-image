/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.display.drawer;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import javax.vecmath.Point2d;
import javax.vecmath.Point2i;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.container.ArrayInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.beanmanager.struct.BooleanProperty;
import io.beanmanager.struct.TreeNode;
import io.beanmanager.struct.TreeRoot;
import io.scenarium.core.struct.BufferedStrategy;
import io.scenarium.core.struct.curve.CurveSeries;
import io.scenarium.core.struct.curve.Curved;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.gui.core.display.ColorProvider;
import io.scenarium.gui.core.display.ScenariumContainer;
import io.scenarium.gui.core.display.StackableDrawer;
import io.scenarium.gui.core.display.drawer.GeometricDrawer;
import io.scenarium.gui.core.display.toolbarclass.ToolBoxItem;
import io.scenarium.image.operator.dataprocessing.image.ImageType;
import io.scenarium.image.operator.dataprocessing.image.conversion.TypeConverter;
import io.scenarium.image.struct.raster.BufferedImageStrategy;
import io.scenarium.image.struct.raster.ByteRaster;
import io.scenarium.image.struct.raster.RGBConverter;
import io.scenarium.image.struct.raster.Raster;
import io.scenarium.image.struct.raster.RasterStrategy;
import io.scenarium.image.struct.raster.ShortRaster;

import javafx.application.Platform;
import javafx.geometry.Dimension2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelFormat;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Affine;

@Deprecated
public class RasterDrawer extends GeometricDrawer implements StackableDrawer {
	private static final String RASTERFILTERS = "Raster";
	private static final String RASTER = "Raster";
	private static final String VALUE = "Value";
	private static int[] grayColorMap;
	static {
		grayColorMap = new int[256];
		for (int i = 0; i < grayColorMap.length; i++)
			grayColorMap[i] = 0xff000000 | i << 16 | i << 8 | i;
	}
	// Filter of Theater
	private boolean filterRaster = true;
	private boolean filterValue = true;
	private final ArrayList<Point2i> selectedCells = new ArrayList<>();
	private final int[][] viewWindow = new int[2][2];
	// Level of details
	protected int lodGrid = 5;
	protected int lodRasterValue1 = 25;
	protected int lodRasterValue3 = 40;
	// Image of Theater
	public boolean computeRaster = true;
	public int thresdholdMin = 0;
	public int thresdholdMax = 255;

	private ColorProvider colorProvider = new ColorProvider();

	private byte[] drawTempArray = new byte[0];
	// private BufferedImage oldDataElement;
	@PropertyInfo(index = 0, info = "Line width for each point of the point cloud")
	@NumberInfo(min = 1)
	private double pointCloudLineWidth = 1;
	@PropertyInfo(index = 1, info = "Line size for each point of the point cloud")
	@NumberInfo(min = 0)
	private double pointCloudLineSize = 5;

	@Override
	protected double adjustScale(double scale) {
		return scale > this.lodGrid ? Math.round(scale) : scale;
	}

	// @Override
	// public void setDrawableElement(Object drawableElement) {
	// super.setDrawableElement(drawableElement);
	// isRasterAlreadyConverted = false;
	// }

	@Override
	public boolean canAddInputToRenderer(Class<?>[] class1) {
		return true;
	}

	private void computeViewWindow() {
		double xTranslate = -getxTranslate();
		double yTranslate = -getyTranslate();
		double scale = getScale();
		int value = (int) (xTranslate / scale);
		this.viewWindow[0][0] = value >= 0 ? value : 0;
		value = (int) (yTranslate / scale);
		this.viewWindow[0][1] = value >= 0 ? value : 0;
		value = (int) ((xTranslate + getWidth() - 1) / scale);
		this.viewWindow[1][0] = value < this.getScenarioWidth() ? value : this.getScenarioWidth() - 1;
		value = (int) ((yTranslate + getHeight() - 1) / scale);
		this.viewWindow[1][1] = value < this.getScenarioHeight() ? value : this.getScenarioHeight() - 1;
	}

	public void drawRaster(ByteRaster raster) {
		int tx = (int) getxTranslate();
		int ty = (int) getyTranslate();
		float scale = (float) getScale();

		int rWid = raster.getWidth();
		int rHei = raster.getHeight();
		int fHei = (int) getHeight();
		int fWid = (int) getWidth();
		PixelFormat<ByteBuffer> pixelFormat = raster.getType() == Raster.GRAY ? PixelFormat.createByteIndexedPremultipliedInstance(grayColorMap) : PixelFormat.getByteRgbInstance();
		if (scale == 1 && rWid == fWid && rHei == fHei)
			this.getGraphicsContext().getPixelWriter().setPixels(tx, ty, raster.getWidth(), raster.getHeight(), pixelFormat, raster.getData(), 0, raster.getWidth() * raster.getDepth());
		else {
			byte[] src = raster.getData();
			double invScale = 1 / scale;
			double xTransScale = tx > 0 ? 0 : -tx * invScale;
			double yTransScale = ty > 0 ? 0 : -ty * invScale;
			int tempArraySize = fWid * fHei * raster.getDepth();
			if (this.drawTempArray.length != tempArraySize)
				this.drawTempArray = new byte[fWid * fHei * raster.getDepth()];
			int pos = (fWid - 1 + (fHei - 1) * fWid) * raster.getDepth();
			int nbToSkipX = 0;
			double boundX = fWid * invScale + xTransScale;
			if (boundX > rWid)
				nbToSkipX = (int) ((boundX - rWid) / invScale);
			int fwidDest = fWid - nbToSkipX;
			int nbToSkipY = 0;
			double boundY = fHei * invScale + yTransScale;
			if (boundY > rHei)
				nbToSkipY = (int) ((boundY - rHei) / invScale);
			int fHeiDest = fHei;
			if (nbToSkipY != 0) {
				fHeiDest -= nbToSkipY;
				pos -= nbToSkipY * fWid * raster.getDepth();
			}
			double idX = (fwidDest - 1) * invScale + xTransScale;
			double idY = (fHeiDest - 1) * invScale + yTransScale;
			if (raster.getType() == Raster.GRAY)
				for (int y = fHeiDest; y-- != 0;) {
					int yOnRaster = (int) idY;
					if (yOnRaster < rHei) {
						int yIndex = yOnRaster * rWid;
						if (nbToSkipX != 0)
							pos -= nbToSkipX;
						double idXC = idX;
						for (int x = fwidDest; x-- != 0;) {
							int index = yIndex + (int) idXC;
							this.drawTempArray[pos] = src[index];
							pos--;
							idXC -= invScale;
						}
					} else
						pos -= fWid;
					idY -= invScale;
				}
			else if (raster.getType() == Raster.RGB) {
				int rWid3 = rWid * 3;
				int riWid3 = fWid * 3;
				for (int y = fHeiDest; y-- != 0;) {
					int yOnRaster = (int) idY;
					if (yOnRaster < rHei) {
						int yIndex = yOnRaster * rWid3;
						if (nbToSkipX != 0)
							pos -= nbToSkipX * 3;
						double idXC = idX;
						for (int x = fwidDest; x-- != 0;) {
							int index = yIndex + 3 * (int) idXC;
							this.drawTempArray[pos + 2] = src[index + 2];
							this.drawTempArray[pos + 1] = src[index + 1];
							this.drawTempArray[pos] = src[index];
							pos -= 3;
							idXC -= invScale;
						}
					} else
						pos -= riWid3;
					idY -= invScale;
				}
			} else {
				int rWid3 = rWid * 3;
				int riWid3 = fWid * 3;
				for (int y = fHeiDest; y-- != 0;) {
					int yOnRaster = (int) idY;
					if (yOnRaster < rHei) {
						int yIndex = yOnRaster * rWid3;
						if (nbToSkipX != 0)
							pos -= nbToSkipX * 3;
						double idXC = idX;
						for (int x = fwidDest; x-- != 0;) {
							int index = yIndex + 3 * (int) idXC;
							this.drawTempArray[pos + 2] = src[index];
							this.drawTempArray[pos] = src[index + 2];
							this.drawTempArray[pos + 1] = src[index + 1];
							pos -= 3;
							idXC -= invScale;
						}
					} else
						pos -= riWid3;
					idY -= invScale;
				}
			}
			int firstX = tx < 0 ? 0 : tx;
			int lastX = tx + (int) (scale * raster.getWidth());
			if (lastX > fWid)
				lastX = fWid;
			int firstY = ty < 0 ? 0 : ty;
			int lastY = ty + (int) (scale * raster.getHeight());
			if (lastY > fHei)
				lastY = fHei;
			this.getGraphicsContext().getPixelWriter().setPixels(firstX, firstY, lastX - firstX, lastY - firstY, pixelFormat, this.drawTempArray, 0, fWid * raster.getDepth());
		}
	}

	@Override
	public Dimension2D getDimension() {
		Object de = getDrawableElement();
		if (de instanceof RasterStrategy)
			return ((RasterStrategy) getDrawableElement()).getDimension();
		else if (de instanceof Raster)
			return new Dimension2D(((Raster) de).getWidth(), ((Raster) de).getHeight());
		else if (de instanceof BufferedImageStrategy)
			return ((BufferedImageStrategy) de).getDimension();
		else
			return new Dimension2D(((BufferedImage) de).getWidth(), ((BufferedImage) de).getHeight());
	}

	@Override
	public float getIntelligentZoomThreshold() {
		return this.lodGrid;
	}

	protected int getLodRasterValue() {
		Object dataElement = getDrawableElement();
		if (dataElement instanceof BufferedStrategy<?>)
			dataElement = ((BufferedStrategy<?>) dataElement).getDrawElement();
		return (dataElement instanceof ByteRaster ? ((ByteRaster) dataElement).getDepth() : ((BufferedImage) dataElement).getColorModel().getPixelSize() / 8) == 1 ? this.lodRasterValue1
				: this.lodRasterValue3;
	}

	@Override
	public ArrayList<ToolBoxItem> getToolBoxItems() {
		ArrayList<ToolBoxItem> list = super.getToolBoxItems();
		list.add(new ToolBoxItem(0, "raster.gif", "Selection raster tools"));
		list.add(new ToolBoxItem(1, "station.gif", "Selection station tools"));
		return list;
	}

	@Override
	public float getZoomScale() {
		return getLodRasterValue() + 1;
	}

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object dataElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, dataElement, autoFitIfResize);
	}

//	@Override
//	protected boolean isAreaSelectionPoint(int x, int y) {
//		return true;
//	}

	@Override
	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput == null ? false
				: Curved.class.isAssignableFrom(additionalInput) || CurveSeries.class.isAssignableFrom(additionalInput) || Point2d[].class.isAssignableFrom(additionalInput);
	}

	@Override
	public void paint(Object dataElement) {
		if (!Platform.isFxApplicationThread())
			throw new IllegalAccessError("This is not Fx Thread");
		GraphicsContext g = this.getGraphicsContext();
		if (!this.filterRaster || getScale() != 1 || getxTranslate() != 0 || getyTranslate() != 0) // efface le theatre si on affiche pas de carte
			clearScreen(g, Color.WHITE);
		Object raster = null;
		if (dataElement instanceof BufferedStrategy<?>)
			dataElement = ((BufferedStrategy<?>) dataElement).getDrawElement();
		if (dataElement instanceof ByteRaster) {
			raster = dataElement;
			if (((ByteRaster) raster).getType() == Raster.BGR) {
				ByteRaster rasterBGR = (ByteRaster) raster;
				byte[] rasterBGRData = rasterBGR.getData();
				ByteRaster rasterRGB = new ByteRaster(rasterBGR.getWidth(), rasterBGR.getHeight(), Raster.RGB);
				byte[] rasterRGBData = rasterRGB.getData();
				for (int i = 0; i < rasterRGBData.length; i += 3) {
					rasterRGBData[i] = rasterBGRData[i + 2];
					rasterRGBData[i + 1] = rasterBGRData[i + 1];
					rasterRGBData[i + 2] = rasterBGRData[i];
				}
				raster = rasterRGB;
			}
		} else if (dataElement instanceof ShortRaster)
			raster = dataElement;
		else {
			BufferedImage img = (BufferedImage) dataElement;
			DataBuffer db = img.getRaster().getDataBuffer();
			if (!(db instanceof DataBufferByte))
				if (img.getType() == BufferedImage.TYPE_USHORT_GRAY)
					raster = new ShortRaster(img.getWidth(), img.getHeight(), Raster.GRAY, ((DataBufferUShort) db).getData());
				else if (img.getColorModel().getColorSpace() instanceof RGBConverter && ((RGBConverter) img.getColorModel().getColorSpace()).needToChangeType())
					raster = ((RGBConverter) img.getColorModel().getColorSpace()).getRaster(img);
				else {
					img = new TypeConverter(ImageType.TYPE_3BYTE_BGR).process(img);
					db = img.getRaster().getDataBuffer();
				}
			if (raster == null) {
				byte[] pixels = ((DataBufferByte) db).getData();
				if (img.getType() == BufferedImage.TYPE_BYTE_GRAY)
					raster = new ByteRaster(img.getWidth(), img.getHeight(), Raster.GRAY, pixels);
				else if (img.getColorModel().getColorSpace() instanceof RGBConverter && ((RGBConverter) img.getColorModel().getColorSpace()).needToChangeType())
					raster = ((RGBConverter) img.getColorModel().getColorSpace()).getRaster(img);
				else // if (!isRasterAlreadyConverted) {
				if (img.getType() == BufferedImage.TYPE_CUSTOM) {
					if (img.getColorModel().getColorSpace() instanceof RGBConverter) {
						pixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData().clone();
						((RGBConverter) img.getColorModel().getColorSpace()).toRGB(pixels);
					}
					raster = new ByteRaster(img.getWidth(), img.getHeight(),
							img.getType() == BufferedImage.TYPE_CUSTOM && img.getColorModel().getColorSpace().getNumComponents() == 1 ? Raster.GRAY : Raster.RGB, pixels);
				} else {
					if (img.getType() != BufferedImage.TYPE_3BYTE_BGR)
						pixels = ((DataBufferByte) new TypeConverter(ImageType.TYPE_3BYTE_BGR).process(img).getRaster().getDataBuffer()).getData();
					raster = new ByteRaster(img.getWidth(), img.getHeight(), Raster.RGB);
					byte[] pixelsDest = ((ByteRaster) raster).getData();
					for (int i = 0; i < pixels.length; i += 3) {
						pixelsDest[i] = pixels[i + 2];
						pixelsDest[i + 1] = pixels[i + 1];
						pixelsDest[i + 2] = pixels[i];
					}
				}
			}
		}
		double scale;
		if (raster instanceof ByteRaster) {
			ByteRaster rasterByte = (ByteRaster) raster;
			if (this.filterRaster)
				drawRaster(rasterByte);
			computeViewWindow();
			scale = getScale();
			if (scale > this.lodRasterValue1 && this.filterValue) {
				g.setFill(Color.BLACK);
				if (rasterByte.getDepth() <= 2 || rasterByte.getDepth() == 3 && scale > this.lodRasterValue3) {
					int xTranslate = (int) getxTranslate();
					int yTranslate = (int) getyTranslate();
					Affine oldTransform = g.getTransform();
					g.setTransform(1, 0, 0, 1, 0, 0);
					g.setTextAlign(TextAlignment.CENTER);
					g.setTextBaseline(VPos.CENTER);
					g.setFont(new Font(12 + (rasterByte.getDepth() == 1 ? 0.52 * (scale - this.lodRasterValue1) : 0.26 * (scale - this.lodRasterValue3))));
					for (int i = this.viewWindow[0][0]; i <= this.viewWindow[1][0]; i++)
						for (int j = this.viewWindow[0][1]; j <= this.viewWindow[1][1]; j++) {
							float gap = 1.0f / (rasterByte.getDepth() + 1);
							for (int k = 0; k < rasterByte.getDepth(); k++) {
								int value = rasterByte.getIntValue(i, j, k);
								if (rasterByte.getDepth() == 1)
									g.setFill(value > 127 ? Color.BLACK : Color.WHITE);
								else if (rasterByte.getType() == Raster.BGR) {
									if (k == 0)
										g.setFill(value > 127 ? new Color(0, 0, 0.5, 1) : Color.BLUE);
									else if (k == 1)
										g.setFill(value > 127 ? new Color(0, 0.5, 0, 1) : Color.LIME);
									else
										g.setFill(value > 127 ? new Color(0.5, 0, 0, 1) : Color.RED);
								} else if (k == 0)
									g.setFill(value > 127 ? new Color(0.5, 0, 0, 1) : Color.RED);
								else if (k == 1)
									g.setFill(value > 127 ? new Color(0, 0.5, 0, 1) : Color.LIME);
								else
									g.setFill(value > 127 ? new Color(0, 0, 0.5, 1) : Color.BLUE);
								g.fillText(Integer.toString(value), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * (k + 1)) + yTranslate));
							}
						}
					g.setTransform(oldTransform);
				}
			}
		} else {
			ShortRaster rasterShort = (ShortRaster) raster;
			if (this.filterRaster) {
				ByteRaster newRaster = new ByteRaster(rasterShort.getWidth(), rasterShort.getHeight(), rasterShort.getType() == Raster.GRAY ? Raster.GRAY : Raster.RGB);
				short[] oriData = rasterShort.getData();
				byte[] newData = newRaster.getData();
				for (int i = 0; i < newData.length; i++)
					newData[i] = (byte) (oriData[i] >> 8);
				drawRaster(newRaster);
			}
			computeViewWindow();
			scale = getScale();
			if (scale > this.lodRasterValue1 && this.filterValue) {
				g.setFill(Color.BLACK);
				if (raster != null && (rasterShort.getDepth() <= 2 || rasterShort.getDepth() == 3 && scale > this.lodRasterValue3)) {
					int xTranslate = (int) getxTranslate();
					int yTranslate = (int) getyTranslate();
					Affine oldTransform = g.getTransform();
					g.setTransform(1, 0, 0, 1, 0, 0);
					g.setTextAlign(TextAlignment.CENTER);
					g.setTextBaseline(VPos.CENTER);
					g.setFont(new Font(10 + 0.26 * (scale - this.lodRasterValue3)));
					for (int i = this.viewWindow[0][0]; i <= this.viewWindow[1][0]; i++)
						for (int j = this.viewWindow[0][1]; j <= this.viewWindow[1][1]; j++) {
							float gap = 1.0f / (rasterShort.getDepth() + 1);
							for (int k = 0; k < rasterShort.getDepth(); k++) {
								int value = rasterShort.getIntValue(i, j, k);
								if (rasterShort.getDepth() == 1)
									g.setFill(value > 127 * 256 ? Color.BLACK : Color.WHITE);
								else if (k == 0)
									g.setFill(value > 127 * 256 ? new Color(0.5, 0, 0, 1) : Color.RED);
								else if (k == 1)
									g.setFill(value > 127 * 256 ? new Color(0, 0.5, 0, 1) : Color.LIME);
								else
									g.setFill(value > 127 * 256 ? new Color(0, 0, 0.5, 1) : Color.BLUE);
								g.fillText(Integer.toString(value), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * (k + 1)) + yTranslate));
							}
						}
					g.setTransform(oldTransform);
				}
			}
		}
		if (this.isFilterGrid() && scale > this.lodGrid) {
			g.setStroke(Color.BLACK);
			g.setLineWidth(1 / getScale());
			int beginX = this.viewWindow[0][0];
			int endX = this.viewWindow[1][0] + 1;
			int beginY = this.viewWindow[0][1];
			int endY = this.viewWindow[1][1] + 1;
			int end = endY;
			for (int i = beginX; i <= endX; i++)
				g.strokeLine(i, beginY, i, end);
			end = endX;
			for (int i = beginY; i <= endY; i++)
				g.strokeLine(beginX, i, end, i);
		} else if (this.isFilterBorder()) {
			g.setStroke(Color.BLACK);
			g.setLineWidth(1 / getScale());
			g.strokeRect(0, 0, this.getScenarioWidth(), this.getScenarioHeight());
		}
		if (this.isFilterROI()) {
			g.setStroke(Color.RED);
			float[] roi = getRoi();
			g.strokeRect(roi[0], roi[1], roi[2], roi[3]);
		}
		Object[] adds = getAdditionalDrawableElement();
		if (adds != null) {
			this.colorProvider.resetIndex();
			g.setLineWidth(this.pointCloudLineWidth / getScale());
			double lineSize = this.pointCloudLineSize;
			for (Object add : adds)
				if (add instanceof Point2d[]) {
					g.setStroke(this.colorProvider.getNextColor());
					for (Point2d point : (Point2d[]) add) {
						double u = point.x;
						double v = point.y;
						g.strokeLine(u + lineSize, v, u - lineSize, v);
						g.strokeLine(u, v - lineSize, u, v + lineSize);
					}
				}
		}
		Point2i[] selectAreaRect = getSelectAreaRect();
		if (selectAreaRect != null) {
			g.setStroke(Color.LIME);
			g.setLineWidth(1 / getScale());
			int x = (int) (selectAreaRect[0].x + 0.5);
			int y = (int) (selectAreaRect[0].y + 0.5);
			int width = (int) Math.round(selectAreaRect[1].x + 0.5);
			int height = (int) Math.round(selectAreaRect[1].y + 0.5);
			g.strokeRect(x, y, width, height);
		}
		// g.setStroke(Color.RED);
		// for (int i = 0; i < getHeight(); i+=30)
		// g.strokeLine(0, i, getWidth(), i);
	}

	@Override
	protected void populateTheaterFilter(TreeRoot<BooleanProperty> theaterFilter) {
		super.populateTheaterFilter(theaterFilter);
		// LinkedHashMap<String, Boolean> filtersMap = new LinkedHashMap<String, Boolean>();
		TreeNode<BooleanProperty> rasNode = new TreeNode<>(new BooleanProperty(RASTERFILTERS, true));
		rasNode.addChild(new TreeNode<>(new BooleanProperty(RASTER, true)));
		rasNode.addChild(new TreeNode<>(new BooleanProperty(VALUE, true)));
		theaterFilter.addChild(rasNode);
		// theaterFilter.put(RASTERFILTERS, filtersMap);
		// filtersMap.put(RASTER, new Boolean(true));
		// filtersMap.put(VALUE, new Boolean(true));
	}

	protected void selectAreaChanged() {}

	@Override
	protected void selectAreaChanged(boolean isCtrlDown) {
		if (this.isSelectArea() && !isCtrlDown)
			this.selectedCells.clear();
	}

	public double getPointCloudLineWidth() {
		return this.pointCloudLineWidth;
	}

	public void setPointCloudLineWidth(double pointCloudLineWidth) {
		this.pointCloudLineWidth = pointCloudLineWidth;
		repaint(false);
	}

	public double getPointCloudLineSize() {
		return this.pointCloudLineSize;
	}

	public void setPointCloudLineSize(double pointCloudLineSize) {
		this.pointCloudLineSize = pointCloudLineSize;
		repaint(false);
	}

	@ArrayInfo(prefHeight = 26 * 8 + 2)
	@PropertyInfo(index = 2, nullable = false, info = "Color for the additional input point cloud")
	public Color[] getPointCloudsColor() {
		return this.colorProvider.getColors();
	}

	public void setPointCloudsColor(Color[] pointCloudColor) {
		this.colorProvider = new ColorProvider(pointCloudColor);
		repaint(false);
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		if (filterPath[filterPath.length - 1].equals(RASTER))
			this.filterRaster = value;
		else if (filterPath[filterPath.length - 1].equals(VALUE))
			this.filterValue = value;
		else
			return super.updateFilterWithPath(filterPath, value);
		repaint(false);
		return true;
	}
}
