/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.display.drawer.imagedrawers;

import java.util.Arrays;

import io.scenarium.image.display.drawer.ImageDrawer;

public class IntegerImageDrawer {

	private IntegerImageDrawer() {}

	public static void drawFullImage(Object imageData, int imageType, int[] viewerData, Number[] range) {
		int[] src = (int[]) imageData;
		int offset = 0;
		switch (imageType) {
		case ImageDrawer.GRAY:
			if (range == null)
				for (int i = 0; i < src.length; i++) {
					byte gray = (byte) (src[i] >> 24);
					viewerData[i] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
				}
			else {
				long min = (Long) range[0];
				long max = (Long) range[1];
				double deltaRange = (min == max ? 1 : max - min) / 255.0;
				for (int i = 0; i < src.length; i++) {
					byte gray = (byte) (((src[i] & 0xFF_FF_FF_FFL) - min) / deltaRange);
					viewerData[i] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
				}
			}
			break;
		case ImageDrawer.RGB:
			if (range == null)
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + (((byte) (src[i] >> 24) & 0xFF) << 16) + (((byte) (src[i + 1] >> 24) & 0xFF) << 8) + ((byte) (src[i + 2] >> 24) & 0xFF);
			else {
				long min = (Long) range[0];
				long max = (Long) range[1];
				double deltaRange = (min == max ? 1 : max - min) / 255.0;
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + (((byte) (((src[i] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF) << 16)
							+ (((byte) (((src[i + 1] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF) << 8) + ((byte) (((src[i + 2] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF);
			}
			break;
		case ImageDrawer.BGR:
			if (range == null)
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + (((byte) (src[i + 2] >> 24) & 0xFF) << 16) + (((byte) (src[i + 1] >> 24) & 0xFF) << 8) + ((byte) (src[i] >> 24) & 0xFF);
			else {
				long min = (Long) range[0];
				long max = (Long) range[1];
				double deltaRange = (min == max ? 1 : max - min) / 255.0;
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + (((byte) (((src[i + 2] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF) << 16)
							+ (((byte) (((src[i + 1] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF) << 8) + ((byte) (((src[i] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF);
			}
			break;
		case ImageDrawer.YUV:
			if (range == null)
				for (int i = 0; i < src.length; i += 3) {
					int yi = src[i];
					int ui = src[i + 1];
					int vi = src[i + 2];
					viewerData[offset++] = (255 << 24) + ((byte) (yi + 1.13983f * vi) << 16) + ((byte) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((byte) (yi + 2.03211f * ui) << 16);
				}
			else {
				long min = (Long) range[0];
				long max = (Long) range[1];
				double deltaRange = (min == max ? 1 : max - min) / 255.0;
				for (int i = 0; i < src.length; i += 3) {
					int yi = (int) ((src[i] - min) / deltaRange);
					int ui = (int) ((src[i + 1] - min) / deltaRange);
					int vi = (int) ((src[i + 2] - min) / deltaRange);
					viewerData[offset++] = (255 << 24) + ((byte) (yi + 1.13983f * vi) << 16) + ((byte) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((byte) (yi + 2.03211f * ui) << 16);
				}
			}
			break;
		case ImageDrawer.ABGR:
			if (range == null)
				for (int i = 0; i < src.length; i++) {
					int argbColor = src[i];
					int r = argbColor >> 16 & 0xFF;
					int b = argbColor & 0xFF;
					viewerData[i] = argbColor & 0xFF00FF00 | b << 16 | r;
				}
			else {
				int min = (int) ((Long) range[0] & 0xFF_FF_FF_FFL);
				int max = (int) ((Long) range[1] & 0xFF_FF_FF_FFL);
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i++) {
					int argbColor = src[i];
					int r = (int) (((argbColor >> 16 & 0xFF) - min) / deltaRange);
					int g = (int) (((argbColor >> 8 & 0xFF) - min) / deltaRange);
					int b = (int) (((argbColor & 0xFF) - min) / deltaRange);
					viewerData[i] = argbColor & 0xFF000000 | b << 16 | g << 8 | r;
				}
			}
			break;
		case ImageDrawer.ARGB:
			if (range == null)
				System.arraycopy(src, 0, viewerData, 0, src.length);
			else {
				int min = (int) ((Long) range[0] & 0xFF_FF_FF_FFL);
				int max = (int) ((Long) range[1] & 0xFF_FF_FF_FFL);
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i++) {
					int argbColor = src[i];
					int r = (int) (((argbColor & 0xFF) - min) / deltaRange);
					// Log.info(r);
					int g = (int) (((argbColor >> 8 & 0xFF) - min) / deltaRange);
					int b = (int) (((argbColor >> 16 & 0xFF) - min) / deltaRange);
					viewerData[i] = argbColor & 0xFF000000 | b << 16 | g << 8 | r;
				}
			}
			break;
		case ImageDrawer.INT_BGR:
			if (range == null)
				for (int i = 0; i < src.length; i++) {
					int argbColor = src[i];
					int r = argbColor >> 16 & 0xFF;
					int b = argbColor & 0xFF;
					viewerData[i] = 0xFF000000 | argbColor & 0x000FF00 | b << 16 | r;
				}
			else {
				int min = (int) ((Long) range[0] & 0xFF_FF_FF_FFL);
				int max = (int) ((Long) range[1] & 0xFF_FF_FF_FFL);
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i++) {
					int argbColor = src[i];
					int r = (int) (((argbColor >> 16 & 0xFF) - min) / deltaRange);
					int g = (int) (((argbColor >> 8 & 0xFF) - min) / deltaRange);
					int b = (int) (((argbColor & 0xFF) - min) / deltaRange);
					viewerData[i] = 0xFF000000 | b << 16 | g << 8 | r;
				}
			}
			break;
		case ImageDrawer.INT_RGB:
			if (range == null)
				for (int i = 0; i < src.length; i++)
					viewerData[i] = 0xFF000000 | src[i];
			else {
				int min = (int) ((Long) range[0] & 0xFF_FF_FF_FFL);
				int max = (int) ((Long) range[1] & 0xFF_FF_FF_FFL);
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i++) {
					int argbColor = src[i];
					int r = (int) (((argbColor & 0xFF) - min) / deltaRange);
					int g = (int) (((argbColor >> 8 & 0xFF) - min) / deltaRange);
					int b = (int) (((argbColor >> 16 & 0xFF) - min) / deltaRange);
					viewerData[i] = 0xFF000000 | b << 16 | g << 8 | r;
				}
			}
			break;
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static void drawImage(Object imageData, int imageType, int imageWidth, int imageHeight, int[] viewerData, int startX, int startY, int width, int height, double startXOnRaster,
			double startYOnRaster, double invScale, int viewerWidth, Number[] range) {
		int[] src = (int[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							byte gray = (byte) (src[yIndex + (int) xOnRaster] >> 24);
							viewerData[yFIndex + x] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				long min = (Long) range[0] & 0xFF_FF_FF_FFL;
				long max = (Long) range[1] & 0xFF_FF_FF_FFL;
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							byte gray = (byte) (((src[yIndex + (int) xOnRaster] & 0xFF_FF_FF_FFL) - min) / deltaRange);
							viewerData[yFIndex + x] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.RGB:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + (((byte) (src[index] >> 24) & 0xFF) << 16) + (((byte) (src[index + 1] >> 24) & 0xFF) << 8) + ((byte) (src[index + 2] >> 24) & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				long min = (Long) range[0];
				long max = (Long) range[1];
				double deltaRange = (min == max ? 1 : max - min) / 255.0;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + (((byte) (((src[index] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF) << 16)
									+ (((byte) (((src[index + 1] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF) << 8) + ((byte) (((src[index + 2] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.BGR:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + (((byte) (src[index + 2] >> 24) & 0xFF) << 16) + (((byte) (src[index + 1] >> 24) & 0xFF) << 8) + ((byte) (src[index] >> 24) & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				long min = (Long) range[0] & 0xFF_FF_FF_FFL;
				long max = (Long) range[1] & 0xFF_FF_FF_FFL;
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + (((byte) (((src[index + 2] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF) << 16)
									+ (((byte) (((src[index + 1] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF) << 8) + ((byte) (((src[index] & 0xFF_FF_FF_FFL) - min) / deltaRange) & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.YUV:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							int yi = src[index];
							int ui = src[index + 1];
							int vi = src[index + 2];
							viewerData[yFIndex + x] = (255 << 24) + ((byte) (yi + 1.13983f * vi) << 16) + ((byte) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((byte) (yi + 2.03211f * ui) << 16);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				long min = (Long) range[0] & 0xFF_FF_FF_FFL;
				long max = (Long) range[1] & 0xFF_FF_FF_FFL;
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							int yi = (int) ((src[index] - min) / deltaRange);
							int ui = (int) ((src[index + 1] - min) / deltaRange);
							int vi = (int) ((src[index + 2] - min) / deltaRange);
							viewerData[yFIndex + x] = (255 << 24) + ((byte) (yi + 1.13983f * vi) << 16) + ((byte) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((byte) (yi + 2.03211f * ui) << 16);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.ABGR:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + (int) xOnRaster;
							int argbColor = src[index];
							int r = argbColor >> 16 & 0xFF;
							int b = argbColor & 0xFF;
							viewerData[yFIndex + x] = argbColor & 0xFF00FF00 | b << 16 | r;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (int) ((Long) range[0] & 0xFF_FF_FF_FFL);
				int max = (int) ((Long) range[1] & 0xFF_FF_FF_FFL);
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + (int) xOnRaster;
							int argbColor = src[index];
							int r = (int) (((argbColor >> 16 & 0xFF) - min) / deltaRange);
							int g = (int) (((argbColor >> 8 & 0xFF) - min) / deltaRange);
							int b = (int) (((argbColor & 0xFF) - min) / deltaRange);
							viewerData[yFIndex + x] = argbColor & 0xFF000000 | b << 16 | g << 8 | r;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.ARGB:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + (int) xOnRaster;
							viewerData[yFIndex + x] = src[index];
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (int) ((Long) range[0] & 0xFF_FF_FF_FFL);
				int max = (int) ((Long) range[1] & 0xFF_FF_FF_FFL);
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + (int) xOnRaster;
							int argbColor = src[index];
							int r = (int) (((argbColor & 0xFF) - min) / deltaRange);
							int g = (int) (((argbColor >> 8 & 0xFF) - min) / deltaRange);
							int b = (int) (((argbColor >> 16 & 0xFF) - min) / deltaRange);
							viewerData[yFIndex + x] = argbColor & 0xFF000000 | b << 16 | g << 8 | r;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.INT_BGR:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + (int) xOnRaster;
							int argbColor = src[index];
							int r = argbColor >> 16 & 0xFF;
							int b = argbColor & 0xFF;
							viewerData[yFIndex + x] = 0xFF000000 | argbColor & 0x000FF00 | b << 16 | r;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (int) ((Long) range[0] & 0xFF_FF_FF_FFL);
				int max = (int) ((Long) range[1] & 0xFF_FF_FF_FFL);
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + (int) xOnRaster;
							int argbColor = src[index];
							int r = (int) (((argbColor >> 16 & 0xFF) - min) / deltaRange);
							int g = (int) (((argbColor >> 8 & 0xFF) - min) / deltaRange);
							int b = (int) (((argbColor & 0xFF) - min) / deltaRange);
							viewerData[yFIndex + x] = 0xFF000000 | b << 16 | g << 8 | r;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.INT_RGB:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + (int) xOnRaster;
							viewerData[yFIndex + x] = 0xFF000000 | src[index];
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (int) ((Long) range[0] & 0xFF_FF_FF_FFL);
				int max = (int) ((Long) range[1] & 0xFF_FF_FF_FFL);
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + (int) xOnRaster;
							int rgbColor = src[index];
							int r = (int) (((rgbColor & 0xFF) - min) / deltaRange);
							int g = (int) (((rgbColor >> 8 & 0xFF) - min) / deltaRange);
							int b = (int) (((rgbColor >> 16 & 0xFF) - min) / deltaRange);
							viewerData[yFIndex + x] = rgbColor & 0xFF000000 | b << 16 | g << 8 | r;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static Number getRGBValue(Object imageData, int imageWidth, int imageType, int x, int y, int z) {
		int[] src = (int[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			return src[y * imageWidth + x] & 0xFF_FF_FF_FFL;
		case ImageDrawer.RGB:
			return src[(y * imageWidth + x) * 3 + z] & 0xFF_FF_FF_FFL;
		case ImageDrawer.BGR:
			return src[(y * imageWidth + x) * 3 + 2 - z] & 0xFF_FF_FF_FFL;
		case ImageDrawer.YUV:
			return src[(y * imageWidth + x) * 3 + z] & 0xFF_FF_FF_FFL;
		case ImageDrawer.ABGR:
			return src[y * imageWidth + x] >> 8 * z & 0xFF;
		case ImageDrawer.ARGB:
			return src[y * imageWidth + x] >> 8 * (2 - z) & 0xFF;
		case ImageDrawer.INT_BGR:
			return src[y * imageWidth + x] >> 8 * z & 0xFF;
		case ImageDrawer.INT_RGB:
			return src[y * imageWidth + x] >> 8 * (2 - z) & 0xFF;
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static Number[] getMinMax(Object imageData, int imageType) {
		int[] src = (int[]) imageData;
		long min, max;
		if (imageType == ImageDrawer.GRAY || imageType == ImageDrawer.RGB || imageType == ImageDrawer.BGR || imageType == ImageDrawer.YUV) {
			min = src[0] & 0xFF_FF_FF_FFL;
			max = src[0] & 0xFF_FF_FF_FFL;
			for (int v : src) {
				long val = v & 0xFF_FF_FF_FFL;
				if (val < min)
					min = val;
				else if (val > max)
					max = val;
			}
		} else {
			min = src[0] & 0xFF;
			max = src[0] & 0xFF;
			for (int val : src) {
				int c = val >> 16 & 0xFF;
				if (c < min)
					min = c;
				else if (c > max)
					max = c;
				c = val >> 8 & 0xFF;
				if (c < min)
					min = c;
				else if (c > max)
					max = c;
				c = val & 0xFF;
				if (c < min)
					min = c;
				else if (c > max)
					max = c;
			}
		}
		return new Number[] { min, max };
	}
}
