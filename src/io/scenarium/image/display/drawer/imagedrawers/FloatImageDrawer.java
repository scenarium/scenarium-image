/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.display.drawer.imagedrawers;

import java.util.Arrays;

import io.scenarium.image.display.drawer.ImageDrawer;

public class FloatImageDrawer {

	private FloatImageDrawer() {}

	public static void drawFullImage(Object imageData, int imageType, int[] viewerData, Number[] range) {
		float[] src = (float[]) imageData;
		int offset = 0;
		switch (imageType) {
		case ImageDrawer.GRAY:
			if (range == null)
				for (int i = 0; i < src.length; i++) {
					byte gray = (byte) (src[i] * 255);
					viewerData[i] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
				}
			else {
				float min = (Float) range[0];
				float max = (Float) range[1];
				float deltaRange = min == max ? 1 : max - min;
				for (int i = 0; i < src.length; i++) {
					byte gray = (byte) ((src[i] - min) / deltaRange * 255);
					viewerData[i] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
				}
			}
			break;
		case ImageDrawer.RGB:
			if (range == null)
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + ((int) (src[i] * 255) << 16) + ((int) (src[i + 1] * 255) << 8) + (int) (src[i + 2] * 255);
			else {
				float min = (Float) range[0];
				float max = (Float) range[1];
				float deltaRange = min == max ? 1 : max - min;
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + ((int) ((src[i] - min) / deltaRange * 255) << 16) + ((int) ((src[i + 1] - min) / deltaRange * 255) << 8)
							+ (int) ((src[i + 2] - min) / deltaRange * 255);
			}
			break;
		case ImageDrawer.BGR:
			if (range == null)
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + ((int) (src[i + 2] * 255) << 16) + ((int) (src[i + 1] * 255) << 8) + (int) (src[i] * 255);
			else {
				float min = (Float) range[0];
				float max = (Float) range[1];
				float deltaRange = min == max ? 1 : max - min;
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + ((int) ((src[i + 2] - min) / deltaRange * 255) << 16) + ((int) ((src[i + 1] - min) / deltaRange * 255) << 8)
							+ (int) ((src[i] - min) / deltaRange * 255);
			}
			break;
		case ImageDrawer.YUV:
			if (range == null)
				for (int i = 0; i < src.length; i += 3) {
					float yi = src[i];
					float ui = src[i + 1];
					float vi = src[i + 2];
					float r = yi + 1.13983f * vi;
					float g = yi - 0.39465f * ui - 0.58060f * vi;
					float b = yi + 2.03211f * ui;
					if (r < 0)
						r = 0;
					else if (r > 1)
						r = 1;
					if (g < 0)
						g = 0;
					else if (g > 1)
						g = 1;
					if (b < 0)
						b = 0;
					else if (b > 1)
						b = 1;
					viewerData[offset++] = (255 << 24) + ((int) (r * 255) << 16) + ((int) (g * 255) << 8) + (int) (b * 255);
				}
			else {
				float min = (Float) range[0];
				float max = (Float) range[1];
				float deltaRange = min == max ? 1 : max - min;
				for (int i = 0; i < src.length; i += 3) {
					float yi = (src[i] - min) / deltaRange;
					float ui = (src[i + 1] - min) / deltaRange;
					float vi = (src[i + 2] - min) / deltaRange;
					float r = (yi + 1.13983f * vi - min) / deltaRange;
					float g = (yi - 0.39465f * ui - 0.58060f * vi - min) / deltaRange;
					float b = (yi + 2.03211f * ui - min) / deltaRange;
					if (r < 0)
						r = 0;
					else if (r > 1)
						r = 1;
					if (g < 0)
						g = 0;
					else if (g > 1)
						g = 1;
					if (b < 0)
						b = 0;
					else if (b > 1)
						b = 1;
					viewerData[offset++] = (255 << 24) + ((int) (r * 255) << 16) + ((int) (g * 255) << 8) + (int) (b * 255);
				}
			}
			break;
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static void drawImage(Object imageData, int imageType, int imageWidth, int imageHeight, int[] viewerData, int startX, int startY, int width, int height, double startXOnRaster,
			double startYOnRaster, double invScale, int viewerWidth, Number[] range) {
		float[] src = (float[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							byte gray = (byte) (src[yIndex + (int) xOnRaster] * 255);
							viewerData[yFIndex + x] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				float min = (Float) range[0];
				float max = (Float) range[1];
				float deltaRange = min == max ? 1 : max - min;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							byte gray = (byte) ((src[yIndex + (int) xOnRaster] - min) / deltaRange * 255);
							viewerData[yFIndex + x] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.RGB:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + ((int) (src[index] * 255) << 16) + ((int) (src[index + 1] * 255) << 8) + (int) (src[index + 2] * 255);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				float min = (Float) range[0];
				float max = (Float) range[1];
				float deltaRange = min == max ? 1 : max - min;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + ((int) ((src[index] - min) / deltaRange * 255) << 16) + ((int) ((src[index + 1] - min) / deltaRange * 255) << 8)
									+ (int) ((src[index + 2] - min) / deltaRange * 255);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.BGR:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + ((int) (src[index + 2] * 255) << 16) + ((int) (src[index + 1] * 255) << 8) + (int) (src[index] * 255);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				float min = (Float) range[0];
				float max = (Float) range[1];
				float deltaRange = min == max ? 1 : max - min;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + ((int) ((src[index + 2] - min) / deltaRange * 255) << 16) + ((int) ((src[index + 1] - min) / deltaRange * 255) << 8)
									+ (int) ((src[index] - min) / deltaRange * 255);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.YUV:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							float yi = src[index];
							float ui = src[index + 1];
							float vi = src[index + 2];
							float r = yi + 1.13983f * vi;
							float g = yi - 0.39465f * ui - 0.58060f * vi;
							float b = yi + 2.03211f * ui;
							if (r < 0)
								r = 0;
							else if (r > 1)
								r = 1;
							if (g < 0)
								g = 0;
							else if (g > 1)
								g = 1;
							if (b < 0)
								b = 0;
							else if (b > 1)
								b = 1;
							viewerData[yFIndex + x] = (255 << 24) + ((int) (r * 255) << 16) + ((int) (g * 255) << 8) + (int) (b * 255);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				float min = (Float) range[0];
				float max = (Float) range[1];
				float deltaRange = min == max ? 1 : max - min;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							float yi = (src[index] - min) / deltaRange;
							float ui = (src[index + 1] - min) / deltaRange;
							float vi = (src[index + 2] - min) / deltaRange;
							float r = (yi + 1.13983f * vi - min) / deltaRange;
							float g = (yi - 0.39465f * ui - 0.58060f * vi - min) / deltaRange;
							float b = (yi + 2.03211f * ui - min) / deltaRange;
							if (r < 0)
								r = 0;
							else if (r > 1)
								r = 1;
							if (g < 0)
								g = 0;
							else if (g > 1)
								g = 1;
							if (b < 0)
								b = 0;
							else if (b > 1)
								b = 1;
							viewerData[yFIndex + x] = (255 << 24) + ((int) (r * 255) << 16) + ((int) (g * 255) << 8) + (int) (b * 255);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static Number getRGBValue(Object imageData, int imageWidth, int imageType, int x, int y, int z) {
		float[] src = (float[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			return src[y * imageWidth + x];
		case ImageDrawer.RGB:
			return src[(y * imageWidth + x) * 3 + z];
		case ImageDrawer.BGR:
			return src[(y * imageWidth + x) * 3 + 2 - z];
		case ImageDrawer.ABGR:
			return src[(y * imageWidth + x) * 4 + 1 + 2 - z];
		case ImageDrawer.YUV:
			return src[(y * imageWidth + x) * 3 + z];
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static Number[] getMinMax(Object imageData, int imageType) {
		float[] src = (float[]) imageData;
		float min = Float.MAX_VALUE;
		float max = -Float.MAX_VALUE;
		for (float val : src)
			if (Float.isFinite(val))
				if (val < min)
					min = val;
				else if (val > max)
					max = val;
		return new Number[] { min, max };
	}
}
