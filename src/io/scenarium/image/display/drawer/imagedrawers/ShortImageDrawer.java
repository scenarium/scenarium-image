/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.display.drawer.imagedrawers;

import java.util.Arrays;

import io.scenarium.image.display.drawer.ImageDrawer;

public class ShortImageDrawer {

	private ShortImageDrawer() {}

	public static void drawFullImage(Object imageData, int imageType, int[] viewerData, Number[] range) {
		short[] src = (short[]) imageData;
		int offset = 0;
		switch (imageType) {
		case ImageDrawer.GRAY:
			if (range == null)
				for (int i = 0; i < src.length; i++) {
					byte gray = (byte) (src[i] >> 8);
					viewerData[i] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i++) {
					byte gray = (byte) (int) ((src[i] & 0xFF_FF - min) / deltaRange);
					viewerData[i] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
				}
			}
			break;
		case ImageDrawer.RGB:
			if (range == null)
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + (((byte) (src[i] >> 8) & 0xFF) << 16) + (((byte) (src[i + 1] >> 8) & 0xFF) << 8) + ((byte) (src[i + 2] >> 8) & 0xFF);
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + (((byte) (((src[i] & 0xFF_FF) - min) / deltaRange) & 0xFF) << 16) + (((byte) (((src[i + 1] & 0xFF_FF) - min) / deltaRange) & 0xFF) << 8)
							+ ((byte) (((src[i + 2] & 0xFF_FF) - min) / deltaRange) & 0xFF);
			}
			break;
		case ImageDrawer.BGR:
			if (range == null)
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + (((byte) (src[i + 2] >> 8) & 0xFF) << 16) + (((byte) (src[i + 1] >> 8) & 0xFF) << 8) + ((byte) (src[i] >> 8) & 0xFF);
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + (((byte) (((src[i + 2] & 0xFF_FF) - min) / deltaRange) & 0xFF) << 16) + (((byte) (((src[i + 1] & 0xFF_FF) - min) / deltaRange) & 0xFF) << 8)
							+ ((byte) (((src[i] & 0xFF_FF) - min) / deltaRange) & 0xFF);
			}
			break;
		case ImageDrawer.YUV:
			if (range == null)
				for (int i = 0; i < src.length; i += 3) {
					int yi = src[i];
					int ui = src[i + 1];
					int vi = src[i + 2];
					viewerData[offset++] = (255 << 24) + ((byte) (yi + 1.13983f * vi) << 16) + ((byte) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((byte) (yi + 2.03211f * ui) << 16);
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i += 3) {
					float yi = (src[i] - min) / deltaRange;
					float ui = (src[i + 1] - min) / deltaRange;
					float vi = (src[i + 2] - min) / deltaRange;
					viewerData[offset++] = (255 << 24) + ((byte) (yi + 1.13983f * vi) << 16) + ((byte) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((byte) (yi + 2.03211f * ui) << 16);
				}
			}
			break;
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static void drawImage(Object imageData, int imageType, int imageWidth, int imageHeight, int[] viewerData, int startX, int startY, int width, int height, double startXOnRaster,
			double startYOnRaster, double invScale, int viewerWidth, Number[] range) {
		short[] src = (short[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							byte gray = (byte) (src[yIndex + (int) xOnRaster] >> 8);
							viewerData[yFIndex + x] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							byte gray = (byte) (int) ((src[yIndex + (int) xOnRaster] & 0xFF_FF - min) / deltaRange);
							viewerData[yFIndex + x] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.RGB:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + (((byte) (src[index] >> 8) & 0xFF) << 16) + (((byte) (src[index + 1] >> 8) & 0xFF) << 8) + ((byte) (src[index + 2] >> 8) & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + (((byte) (((src[index] & 0xFF_FF) - min) / deltaRange) & 0xFF) << 16)
									+ (((byte) (((src[index + 1] & 0xFF_FF) - min) / deltaRange) & 0xFF) << 8) + ((byte) (((src[index + 2] & 0xFF_FF) - min) / deltaRange) & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.BGR:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + (((byte) (src[index + 2] >> 8) & 0xFF) << 16) + (((byte) (src[index + 1] >> 8) & 0xFF) << 8) + ((byte) (src[index] >> 8) & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + (((byte) (((src[index + 2] & 0xFF_FF) - min) / deltaRange) & 0xFF) << 16)
									+ (((byte) (((src[index + 1] & 0xFF_FF) - min) / deltaRange) & 0xFF) << 8) + ((byte) (((src[index] & 0xFF_FF) - min) / deltaRange) & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.YUV:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							int yi = src[index];
							int ui = src[index + 1];
							int vi = src[index + 2];
							viewerData[yFIndex + x] = (255 << 24) + ((byte) (yi + 1.13983f * vi) << 16) + ((byte) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((byte) (yi + 2.03211f * ui) << 16);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							float yi = (src[index] - min) / deltaRange;
							float ui = (src[index + 1] - min) / deltaRange;
							float vi = (src[index + 2] - min) / deltaRange;
							viewerData[yFIndex + x] = (255 << 24) + ((byte) (yi + 1.13983f * vi) << 16) + ((byte) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((byte) (yi + 2.03211f * ui) << 16);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static Number getRGBValue(Object imageData, int imageWidth, int imageType, int x, int y, int z) {
		short[] src = (short[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			return src[y * imageWidth + x] & 0xFFFF;
		case ImageDrawer.RGB:
			return src[(y * imageWidth + x) * 3 + z] & 0xFFFF;
		case ImageDrawer.BGR:
			return src[(y * imageWidth + x) * 3 + 2 - z] & 0xFFFF;
		case ImageDrawer.ABGR:
			return src[(y * imageWidth + x) * 4 + 1 + 2 - z] & 0xFFFF;
		case ImageDrawer.YUV:
			return src[(y * imageWidth + x) * 3 + z] & 0xFFFF;
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static Number[] getMinMax(Object imageData, int imageType) {
		short[] src = (short[]) imageData;
		int min = src[0] & 0xFFFF;
		int max = src[0] & 0xFFFF;
		for (short v : src) {
			int val = v & 0xFFFF;
			if (val < min)
				min = val;
			else if (val > max)
				max = val;
		}
		return new Number[] { min, max };
	}
}
