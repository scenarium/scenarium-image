/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.display.drawer;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.lang.reflect.Array;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.vecmath.Point2d;
import javax.vecmath.Point2i;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.container.ArrayInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.beanmanager.struct.BooleanProperty;
import io.beanmanager.struct.TreeNode;
import io.beanmanager.struct.TreeRoot;
import io.scenarium.core.struct.BufferedStrategy;
import io.scenarium.core.struct.curve.CurveSeries;
import io.scenarium.core.struct.curve.Curved;
import io.scenarium.gui.core.display.ColorProvider;
import io.scenarium.gui.core.display.StackableDrawer;
import io.scenarium.gui.core.display.drawer.GeometricDrawer;
import io.scenarium.image.display.drawer.imagedrawers.ByteImageDrawer;
import io.scenarium.image.display.drawer.imagedrawers.DoubleImageDrawer;
import io.scenarium.image.display.drawer.imagedrawers.FloatImageDrawer;
import io.scenarium.image.display.drawer.imagedrawers.IntegerImageDrawer;
import io.scenarium.image.display.drawer.imagedrawers.ShortImageDrawer;
import io.scenarium.image.internal.Log;
import io.scenarium.image.operator.dataprocessing.image.ImageType;
import io.scenarium.image.operator.dataprocessing.image.conversion.TypeConverter;
import io.scenarium.image.struct.raster.BufferedImageStrategy;
import io.scenarium.image.struct.raster.ByteRaster;
import io.scenarium.image.struct.raster.IntegerRaster;
import io.scenarium.image.struct.raster.Raster;
import io.scenarium.image.struct.raster.RasterStrategy;

import javafx.beans.InvalidationListener;
import javafx.geometry.Dimension2D;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelBuffer;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Affine;

public class ImageDrawer extends GeometricDrawer implements StackableDrawer {
	public static final int GRAY = 0;
	public static final int RGB = 1;
	public static final int BGR = 2;
	public static final int YUV = 3;
	public static final int ARGB = 4;
	public static final int ABGR = 5;
	public static final int INT_RGB = 6;
	public static final int INT_BGR = 7;
	private static final HashMap<Class<?>, ImageDrawerInfo> IMAGE_DRAWERS = new HashMap<>();
	private static final String RASTER_FILTERS = "Raster";
	private static final String RASTER = "Raster";
	private static final String VALUE = "Value";
	private static final int[] GRAY_COLOR_MAP;
	@PropertyInfo(index = 0, info = "Line width for each point of the point cloud")
	@NumberInfo(min = 1)
	private double pointCloudLineWidth = 1;
	@PropertyInfo(index = 1, info = "Line size for each point of the point cloud")
	@NumberInfo(min = 0)
	private double pointCloudLineSize = 5;
	@PropertyInfo(index = 2, info = "Normalize the drawn raster")
	private boolean normalize;
	private ColorProvider colorProvider = new ColorProvider();

	// Element to draw image
	private PixelBuffer<IntBuffer> pixelBuffer;
	private final ImageView imageView;
	private Rectangle2D imageRegion;

	// Filter of Theater
	private boolean filterRaster = true;
	private boolean filterValue = true;
	private final ArrayList<Point2i> selectedCells = new ArrayList<>();
	private final int[] viewWindow = new int[2 * 2];

	// Level of details
	private final int lodGrid = 5;
	private final int lodRasterValue1 = 15;
	private final int lodRasterValue3 = 25;
	// Image of Theater
	private boolean isRasterDrawn;
	private boolean needToRefreshGc;
	private Number[] range;
	private boolean selectAreaRectDrawn = false;

	static {
		IMAGE_DRAWERS.put(byte[].class, new ImageDrawerInfo(ByteImageDrawer::drawFullImage, ByteImageDrawer::drawImage, ByteImageDrawer::getRGBValue, ByteImageDrawer::getMinMax));
		IMAGE_DRAWERS.put(short[].class, new ImageDrawerInfo(ShortImageDrawer::drawFullImage, ShortImageDrawer::drawImage, ShortImageDrawer::getRGBValue, ShortImageDrawer::getMinMax));
		IMAGE_DRAWERS.put(int[].class, new ImageDrawerInfo(IntegerImageDrawer::drawFullImage, IntegerImageDrawer::drawImage, IntegerImageDrawer::getRGBValue, IntegerImageDrawer::getMinMax));
		IMAGE_DRAWERS.put(float[].class, new ImageDrawerInfo(FloatImageDrawer::drawFullImage, FloatImageDrawer::drawImage, FloatImageDrawer::getRGBValue, FloatImageDrawer::getMinMax));
		IMAGE_DRAWERS.put(double[].class, new ImageDrawerInfo(DoubleImageDrawer::drawFullImage, DoubleImageDrawer::drawImage, DoubleImageDrawer::getRGBValue, DoubleImageDrawer::getMinMax));
		GRAY_COLOR_MAP = new int[256];
		for (int i = 0; i < GRAY_COLOR_MAP.length; i++)
			GRAY_COLOR_MAP[i] = 0xff000000 | i << 16 | i << 8 | i;
	}

	public ImageDrawer() {
		this.imageView = new ImageView();
		InvalidationListener il = b -> {
			int width = (int) getWidth();
			int height = (int) getHeight();
			if (width <= 0 || height <= 0)
				return;
			this.pixelBuffer = new PixelBuffer<>(width, height, IntBuffer.wrap(new int[width * height]), PixelFormat.getIntArgbPreInstance());
			this.imageRegion = new Rectangle2D(0, 0, width, height);
			this.imageView.setImage(new WritableImage(this.pixelBuffer));
			paintImmediately(false);
		};
		il.invalidated(null);
		widthProperty().addListener(il);
		heightProperty().addListener(il);
		this.imageView.fitWidthProperty().bind(widthProperty());
		this.imageView.fitHeightProperty().bind(heightProperty());
		getChildren().add(0, this.imageView);
	}

	@Override
	public void scale1AndReplace() {
		super.scale1AndReplace();
		updateTransform(1, 0, 0);
	}

	@Override
	public Dimension2D getDimension() {
		Object de = getDrawableElement();
		if (de instanceof RasterStrategy)
			return ((RasterStrategy) getDrawableElement()).getDimension();
		else if (de instanceof Raster)
			return new Dimension2D(((Raster) de).getWidth(), ((Raster) de).getHeight());
		else if (de instanceof BufferedImageStrategy)
			return ((BufferedImageStrategy) de).getDimension();
		else
			return new Dimension2D(((BufferedImage) de).getWidth(), ((BufferedImage) de).getHeight());
	}

	@Override
	public void setDrawableElement(Object drawableElement) {
		super.setDrawableElement(drawableElement);
		this.range = null;
	}

	@Override
	public float getIntelligentZoomThreshold() {
		return this.getLodGrid();
	}

	@Override
	public float getZoomScale() {
		return getLodRasterValue() + 1;
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		this.setNeedToRefreshGc(true);
		if (filterPath[filterPath.length - 1].equals(RASTER))
			this.filterRaster = value;
		else if (filterPath[filterPath.length - 1].equals(VALUE))
			this.filterValue = value;
		else
			return super.updateFilterWithPath(filterPath, value);
		repaint(false);
		return true;
	}

	@Override
	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput == null ? false
				: Curved.class.isAssignableFrom(additionalInput) || CurveSeries.class.isAssignableFrom(additionalInput) || Point2d[].class.isAssignableFrom(additionalInput);
	}

	@Override
	public boolean canAddInputToRenderer(Class<?>[] class1) {
		return true;
	}

	public double getPointCloudLineWidth() {
		return this.pointCloudLineWidth;
	}

	public void setPointCloudLineWidth(double pointCloudLineWidth) {
		var oldValue = this.pointCloudLineWidth;
		this.pointCloudLineWidth = pointCloudLineWidth;
		repaint(false);
		this.pcs.firePropertyChange("pointCloudLineWidth", oldValue, this.pointCloudLineWidth);
	}

	public double getPointCloudLineSize() {
		return this.pointCloudLineSize;
	}

	public void setPointCloudLineSize(double pointCloudLineSize) {
		var oldValue = this.pointCloudLineSize;
		this.pointCloudLineSize = pointCloudLineSize;
		repaint(false);
		this.pcs.firePropertyChange("pointCloudLineSize", oldValue, this.pointCloudLineSize);
	}

	public boolean isNormalize() {
		return this.normalize;
	}

	public void setNormalize(boolean normalize) {
		var oldValue = this.normalize;
		this.normalize = normalize;
		if (!normalize)
			this.range = null;
		repaint(false);
		this.pcs.firePropertyChange("normalize", oldValue, this.normalize);
	}

	@ArrayInfo(prefHeight = 26 * 8 + 2)
	@PropertyInfo(index = 2, nullable = false, info = "Color for the additional input point cloud")
	public Color[] getPointCloudsColor() {
		return this.colorProvider.getColors();
	}

	public void setPointCloudsColor(Color[] pointCloudsColor) {
		var oldValue = getPointCloudsColor();
		this.colorProvider = new ColorProvider(pointCloudsColor);
		repaint(false);
		this.pcs.firePropertyChange("pointCloudsColor", oldValue, pointCloudsColor);
	}

	@Override
	protected void paint(Object de) {
		Object dataElement = de instanceof BufferedStrategy<?> ? ((BufferedStrategy<?>) de).getDrawElement() : de;
		if (dataElement instanceof BufferedImage) {
			BufferedImage image = (BufferedImage) dataElement;
			int imgType = image.getType();
			if (imgType == BufferedImage.TYPE_BYTE_BINARY || imgType == BufferedImage.TYPE_BYTE_INDEXED || imgType == BufferedImage.TYPE_USHORT_555_RGB || imgType == BufferedImage.TYPE_USHORT_565_RGB
					|| imgType == BufferedImage.TYPE_CUSTOM) {
				paint(new TypeConverter(imgType == BufferedImage.TYPE_BYTE_BINARY ? ImageType.TYPE_BYTE_GRAY : ImageType.TYPE_INT_ARGB).process(image));
				return;
			}
		}

		int imageWidth, imageHeight, imageType;
		Object imageData;
		if (dataElement instanceof BufferedImage) {
			BufferedImage image = (BufferedImage) dataElement;
			imageWidth = image.getWidth();
			imageHeight = image.getHeight();
			DataBuffer db = image.getRaster().getDataBuffer();
			imageData = db instanceof DataBufferByte ? ((DataBufferByte) db).getData()
					: db instanceof DataBufferShort ? ((DataBufferShort) db).getData() : db instanceof DataBufferUShort ? ((DataBufferUShort) db).getData() : ((DataBufferInt) db).getData();
			switch (image.getType()) {
			case BufferedImage.TYPE_BYTE_GRAY:
			case BufferedImage.TYPE_USHORT_GRAY:
				imageType = GRAY;
				break;
			case BufferedImage.TYPE_3BYTE_BGR:
				imageType = BGR;
				break;
			case BufferedImage.TYPE_4BYTE_ABGR:
				imageType = ABGR;
				break;
			case BufferedImage.TYPE_4BYTE_ABGR_PRE:
				imageType = ABGR;
				break;
			case BufferedImage.TYPE_INT_ARGB:
				imageType = ARGB;
				break;
			case BufferedImage.TYPE_INT_ARGB_PRE:
				imageType = ARGB;
				break;
			case BufferedImage.TYPE_INT_BGR:
				imageType = INT_BGR;
				break;
			case BufferedImage.TYPE_INT_RGB:
				imageType = INT_RGB;
				break;
			default:
				imageType = 0;
			}
		} else {
			Raster image = (Raster) dataElement;
			imageWidth = image.getWidth();
			imageHeight = image.getHeight();
			imageData = image.getData();
			switch (image.getType()) {
			case Raster.GRAY:
				imageType = GRAY;
				break;
			case Raster.BGR:
				imageType = BGR;
				break;
			case Raster.RGB:
				imageType = RGB;
				break;
			case Raster.YUV:
				imageType = YUV;
				break;
			case IntegerRaster.ARGB8:
				imageType = ARGB;
				break;
			case IntegerRaster.ABGR8:
				imageType = ABGR;
				break;
			default:
				imageType = 0;
			}
		}
		ImageDrawerInfo idi = IMAGE_DRAWERS.get(imageData.getClass());
		if (this.range == null && this.normalize && Array.getLength(imageData) != 0)
			this.range = idi.getMinMaxProvider().getMinMax(imageData, imageType);
		if (this.pixelBuffer != null && (this.filterRaster || this.isRasterDrawn))
			this.pixelBuffer.updateBuffer(pb -> {
				double scale = getScale();
				double tx = getxTranslate();
				double ty = getyTranslate();
				int viewerHeight = (int) getHeight();
				int viewerWidth = (int) getWidth();
				// occurs when the size of the node changed but the resize event is not yet occurred. The next resize event while refresh the view.
				if (!isValidTransform(viewerWidth, viewerHeight, imageWidth, imageHeight, scale, tx, ty))
					return this.imageRegion;
				int[] viewerData = pb.getBuffer().array();
				if (!this.filterRaster) {
					Arrays.fill(viewerData, 0);
					this.isRasterDrawn = false;
					return this.imageRegion;
				}
				if (imageWidth == 0 || imageHeight == 0 || viewerHeight == 0 || viewerWidth == 0)
					Arrays.fill(viewerData, 0);
				else {
					if (scale == 1 && imageWidth == viewerWidth && imageHeight == viewerHeight && tx == 0 && ty == 0)
						idi.getFirst().drawImage(imageData, imageType, viewerData, this.range);
					else
						try {
							double invScale = 1 / scale;
							double xTranslate = -tx * invScale;
							double yTranslate = -ty * invScale;
							int pos = viewerWidth - 1 + (viewerHeight - 1) * viewerWidth;
							int nbToSkipX = 0;
							double boundX = viewerWidth * invScale + xTranslate;
							if (boundX > imageWidth)
								nbToSkipX = (int) Math.round((boundX - imageWidth) / invScale);
							int width = viewerWidth - nbToSkipX;
							int endY = 0;
							double boundY = viewerHeight * invScale + yTranslate;
							if (boundY > imageHeight)
								endY = (int) ((boundY - imageHeight) * scale);
							int startX = (int) Math.ceil(-xTranslate * scale);
							int startY = (int) Math.ceil(-yTranslate * scale);
							if (startY > 0)
								Arrays.fill(viewerData, 0, startY * viewerWidth, 0);
							int height = viewerHeight;
							if (endY != 0) {
								height -= endY;
								pos -= endY * viewerWidth;
								Arrays.fill(viewerData, pos, pos + endY * viewerWidth, 0);
							}
							double startXOnRaster = (width - 1) * invScale + xTranslate;
							double startYOnRaster = (height - 1) * invScale + yTranslate;
							idi.getSecond().drawImage(imageData, imageType, imageWidth, imageHeight, viewerData, Math.max(0, startX), Math.max(0, startY), width, height, startXOnRaster,
									startYOnRaster, invScale, viewerWidth, this.range);
						} catch (ArrayIndexOutOfBoundsException e) {
							Log.error("An error occured while drawing image.\n\tImage: " + imageWidth + "x" + imageHeight + "(" + Array.getLength(imageData) + ")" + " t: " + imageType + "\n\tViewer: "
									+ viewerWidth + "x" + viewerHeight + "(" + viewerData.length + ")" + "\n\tTransformation: scale: " + scale + " translation: (" + tx + "," + ty + ")");
							e.printStackTrace();
						}
					this.isRasterDrawn = true;
				}
				drawAdditionalElements(dataElement, imageData, imageWidth, imageHeight, imageType, idi);
				return this.imageRegion;
			});
		else
			drawAdditionalElements(dataElement, imageData, imageWidth, imageHeight, imageType, idi);
	}

	@Override
	protected void populateTheaterFilter(TreeRoot<BooleanProperty> theaterFilter) {
		super.populateTheaterFilter(theaterFilter);
		TreeNode<BooleanProperty> rasNode = new TreeNode<>(new BooleanProperty(RASTER_FILTERS, true));
		rasNode.addChild(new TreeNode<>(new BooleanProperty(RASTER, true)));
		rasNode.addChild(new TreeNode<>(new BooleanProperty(VALUE, true)));
		theaterFilter.addChild(rasNode);
	}

	@Override
	protected double adjustScale(double scale) {
		return scale > this.getLodGrid() ? Math.round(scale) : scale;
	}

	@Override
	protected void updateTransform(double scale, double tx, double ty) {
		if (scale != getScale() || tx != getxTranslate() || ty != getyTranslate())
			this.setNeedToRefreshGc(true);
		super.updateTransform(scale, tx, ty);
	}

//	@Override
//	protected boolean isAreaSelectionPoint(int x, int y) {
//		return true;
//	}

	@Override
	protected void selectAreaChanged(boolean isCtrlDown) {
		if (this.isSelectArea() && !isCtrlDown)
			this.selectedCells.clear();
		this.setNeedToRefreshGc(true);
	}

	protected int[] getViewWindow() {
		return this.viewWindow.clone();
	}

	protected void clearScreen(GraphicsContext g) {
		Affine oldTransform = g.getTransform();
		g.setTransform(1, 0, 0, 1, 0, 0);
		g.clearRect(0, 0, getWidth(), getHeight());
		g.setTransform(oldTransform);
	}

	protected int getLodRasterValue() {
		Object dataElement = getDrawableElement();
		if (dataElement instanceof BufferedStrategy<?>)
			dataElement = ((BufferedStrategy<?>) dataElement).getDrawElement();
		return (dataElement instanceof ByteRaster ? ((ByteRaster) dataElement).getDepth() : ((BufferedImage) dataElement).getColorModel().getPixelSize() / 8) == 1 ? this.lodRasterValue1
				: this.lodRasterValue3;
	}

	private void drawAdditionalElements(Object dataElement, Object imageData, int imageWidth, int imageHeight, int imageType, ImageDrawerInfo idi) {
		double scale = getScale();
		boolean viewWindowsComputed = false;
		// Draw values
		GraphicsContext gc = this.getGraphicsContext();
		Object[] adds = getAdditionalDrawableElement();
		boolean needToRefresh = this.isNeedToRefreshGc() || adds != null && adds.length != 0 || this.isFilterROI() || this.selectAreaRectDrawn && this.getSelectAreaRect() == null;
		this.setNeedToRefreshGc(false);
		boolean clearRectDone = false;
		ImageValueProvider ivp = idi.getImageValueProvider();
		Class<?> elementType = imageType == ARGB || imageType == ABGR || imageType == INT_BGR || imageType == INT_RGB ? byte.class : imageData.getClass().getComponentType();
		int nbChannel = dataElement instanceof BufferedImage
				? ((BufferedImage) dataElement).getType() == BufferedImage.TYPE_BYTE_GRAY || ((BufferedImage) dataElement).getType() == BufferedImage.TYPE_USHORT_GRAY ? 1 : 3
				: ((Raster) dataElement).getNbChannel();
		int lod = getLevelOfDetails(elementType, nbChannel);
		if (this.filterValue && scale > lod) {
			gc.setFont(new Font(getFontScaleFactor(elementType, nbChannel) * scale));
			gc.setFill(Color.BLACK);
			this.setNeedToRefreshGc(true);
			if (!clearRectDone) {
				clearScreen(gc);
				clearRectDone = true;
			}
			if (!viewWindowsComputed) {
				computeViewWindow();
				viewWindowsComputed = true;
			}
			int xTranslate = (int) getxTranslate();
			int yTranslate = (int) getyTranslate();
			Affine oldTransform = gc.getTransform();
			gc.setTransform(1, 0, 0, 1, 0, 0);
			gc.setTextAlign(TextAlignment.CENTER);
			gc.setTextBaseline(VPos.CENTER);
			double min;
			double max;
			double deltaRange;
			if (this.range != null) {
				min = this.range[0].doubleValue();
				max = this.range[1].doubleValue();
				deltaRange = min == max ? 1 : max - min;
			} else if (elementType == int.class) {
				min = 0;
				max = Integer.MAX_VALUE * 2L + 1L;
				deltaRange = max - min;
			} else if (elementType == short.class) {
				min = 0;
				max = Short.MAX_VALUE * 2 + 1;
				deltaRange = max - min;
			} else if (elementType == byte.class) {
				min = 0;
				max = Byte.MAX_VALUE * 2 + 1;
				deltaRange = max - min;
			} else {
				min = 0;
				max = 1;
				deltaRange = 1;
			}
			for (int i = this.viewWindow[0]; i <= this.viewWindow[2]; i++)
				for (int j = this.viewWindow[1]; j <= this.viewWindow[3]; j++)
					if (nbChannel == 1) {
						double value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 0).doubleValue();
						gc.setFill((value - min) / deltaRange > 0.5 ? Color.BLACK : Color.WHITE);
						gc.fillText(formatFloatValue(elementType, value, false), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + 0.5) + yTranslate));
					} else if (nbChannel == 3) {
						float gap = 1.0f / (nbChannel + 1);
						if (imageType != YUV) {
							double value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 0).doubleValue();
							gc.setFill((value - min) / deltaRange > 0.5 ? new Color(0.5, 0, 0, 1) : Color.RED);
							gc.fillText(formatFloatValue(elementType, value, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 1) + yTranslate));
							value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 1).doubleValue();
							gc.setFill((value - min) / deltaRange > 0.5 ? new Color(0, 0.5, 0, 1) : Color.LIME);
							gc.fillText(formatFloatValue(elementType, value, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 2) + yTranslate));
							value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 2).doubleValue();
							gc.setFill((value - min) / deltaRange > 0.5 ? new Color(0, 0, 0.5, 1) : Color.BLUE);
							gc.fillText(formatFloatValue(elementType, value, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 3) + yTranslate));
						} else {
							double y = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 0).doubleValue();
							gc.setFill((y - min) / deltaRange > 0.5 ? Color.BLACK : Color.WHITE);
							gc.fillText(formatFloatValue(elementType, y, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 1) + yTranslate));
							double u = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 1).doubleValue();
							gc.fillText(formatFloatValue(elementType, u, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 2) + yTranslate));
							double v = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 2).doubleValue();
							gc.fillText(formatFloatValue(elementType, v, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 3) + yTranslate));
						}
					}
			gc.setTransform(oldTransform);
			needToRefresh = true;
		}

		if (!clearRectDone && needToRefresh)
			clearScreen(gc);
		if (needToRefresh) {
			double eps = 0.5 / scale;
			if (this.isFilterGrid() && scale > this.getLodGrid()) {
				if (!viewWindowsComputed)
					computeViewWindow();
				gc.setStroke(Color.BLACK);
				gc.setLineWidth(1 / getScale());
				int beginX = this.viewWindow[0];
				int endX = this.viewWindow[2] + 1;
				int beginY = this.viewWindow[1];
				int endY = this.viewWindow[3] + 1;
				int end = endY;
				for (int i = beginX; i <= endX; i++)
					gc.strokeLine(i + eps, beginY + eps, i + eps, end + eps);
				end = endX;
				for (int i = beginY; i <= endY; i++)
					gc.strokeLine(beginX + eps, i + eps, end + eps, i + eps);
			} else if (this.isFilterBorder()) {
				gc.setStroke(Color.BLACK);
				gc.setLineWidth(1 / getScale());
				gc.strokeRect(0, 0, this.getScenarioWidth(), this.getScenarioHeight());
			}
			if (this.isFilterROI()) {
				gc.setStroke(Color.RED);
				float[] roi = getRoi();
				gc.strokeRect(roi[0] + eps, roi[1] + eps, roi[2] + eps, roi[3] + eps);
			}
			if (adds != null) {
				this.colorProvider.resetIndex();
				gc.setLineWidth(this.pointCloudLineWidth / getScale());
				double lineSize = this.pointCloudLineSize;
				for (Object add : adds)
					if (add instanceof Point2d[]) {
						gc.setStroke(this.colorProvider.getNextColor());
						for (Point2d point : (Point2d[]) add)
							if (add != null) {
								double u = point.x;
								double v = point.y;
								gc.strokeLine(u + lineSize, v, u - lineSize, v);
								gc.strokeLine(u, v - lineSize, u, v + lineSize);
							}
					}
			}
			Point2i[] selectAreaRect = getSelectAreaRect();
			if (selectAreaRect != null) {
				this.selectAreaRectDrawn = true;
				gc.setStroke(Color.LIME);
				gc.setLineWidth(1 / getScale());
				int x = selectAreaRect[0].x;
				int y = selectAreaRect[0].y;
				int width = selectAreaRect[1].x + 1;
				int height = selectAreaRect[1].y + 1;
				gc.strokeRect(x + eps, y + eps, width, height);
			} else
				this.selectAreaRectDrawn = false;
		}
	}

	private void computeViewWindow() {
		double xTranslate = -getxTranslate();
		double yTranslate = -getyTranslate();
		double scale = getScale();
		int value = (int) (xTranslate / scale);
		this.viewWindow[0] = value >= 0 ? value : 0;
		value = (int) (yTranslate / scale);
		this.viewWindow[1] = value >= 0 ? value : 0;
		value = (int) ((xTranslate + getWidth() - 1) / scale);
		this.viewWindow[2] = value < this.getScenarioWidth() ? value : this.getScenarioWidth() - 1;
		value = (int) ((yTranslate + getHeight() - 1) / scale);
		this.viewWindow[3] = value < this.getScenarioHeight() ? value : this.getScenarioHeight() - 1;
	}

	private static int getLevelOfDetails(Class<?> type, int nbChannel) {
		if (type == double.class || type == float.class || type == int.class)
			return nbChannel == 1 ? 20 : 40;
		if (type == short.class)
			return 25;
		return nbChannel == 1 ? 15 : 25;
	}

	private static double getFontScaleFactor(Class<?> type, int nbChannel) {
		if (type == double.class || type == float.class || type == int.class)
			return nbChannel == 1 ? 0.3 : 0.16;
		if (type == short.class)
			return 0.25;
		return nbChannel == 1 ? 0.4 : 0.25;
	}

	private static String formatFloatValue(Class<?> type, double value, boolean oneLine) {
		String text;
		if (type == double.class || type == float.class || type == int.class) {
			if (Double.isFinite(value)) {
				text = String.format("%.3e", value);
				if (!oneLine) {
					int eIndex = text.indexOf("e");
					if (eIndex != -1)
						text = text.substring(0, eIndex) + System.lineSeparator() + text.substring(eIndex);
				}
			} else if (value == Double.POSITIVE_INFINITY)
				text = Character.valueOf((char) 8734).toString();
			else if (value == Double.NEGATIVE_INFINITY)
				text = "-" + Character.valueOf((char) 8734).toString();
			else
				text = Double.toString(value);
		} else
			text = Long.toString((long) value);
		return text;
	}

	public int getLodGrid() {
		return this.lodGrid;
	}

	public boolean isNeedToRefreshGc() {
		return this.needToRefreshGc;
	}

	public void setNeedToRefreshGc(boolean needToRefreshGc) {
		this.needToRefreshGc = needToRefreshGc;
	}
}

@FunctionalInterface
interface NominalDrawer {
	public void drawImage(Object src, int srcType, int[] dest, Number[] range);
}

@FunctionalInterface
interface Drawer {
	public void drawImage(Object src, int srcType, int srcWid, int srcHei, int[] dest, int startX, int startY, int width, int height, double startXOnRaster, double startYOnRaster, double invScale,
			int frameWidht, Number[] range);
}

@FunctionalInterface
interface ImageValueProvider {
	public Number getRGBValue(Object imageData, int imageWidth, int imageType, int x, int y, int z);
}

@FunctionalInterface
interface MinMaxProvider {
	public Number[] getMinMax(Object imageData, int imageType);
}

class ImageDrawerInfo {
	private final NominalDrawer fullImageDrawer;
	private final Drawer drawer;
	private final ImageValueProvider imageValueProvider;
	private final MinMaxProvider minMaxProvider;

	public ImageDrawerInfo(NominalDrawer first, Drawer second, ImageValueProvider imageValueProvider, MinMaxProvider minMaxProvider) {
		this.fullImageDrawer = first;
		this.drawer = second;
		this.imageValueProvider = imageValueProvider;
		this.minMaxProvider = minMaxProvider;
	}

	public NominalDrawer getFirst() {
		return this.fullImageDrawer;
	}

	public Drawer getSecond() {
		return this.drawer;
	}

	public ImageValueProvider getImageValueProvider() {
		return this.imageValueProvider;
	}

	public MinMaxProvider getMinMaxProvider() {
		return this.minMaxProvider;
	}
}
