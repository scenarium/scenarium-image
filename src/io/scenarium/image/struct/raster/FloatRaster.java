/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.struct.raster;

import java.util.Arrays;

public class FloatRaster implements Raster {
	private final float[] raster;
	private final int width, height, depth, widDep;

	private final int type;

	public FloatRaster(int width, int height, int type) {
		this(width, height, type, null);
	}

	public FloatRaster(int width, int height, int type, float[] pixels) {
		if (width < 0)
			throw new IllegalArgumentException("The width of the raster must be >= 0");
		if (height < 0)
			throw new IllegalArgumentException("The height of the raster must be >= 0");
		if (type < GRAY || type > YUV)
			throw new IllegalArgumentException("The type of the raster does not exists");
		this.width = width;
		this.height = height;
		this.depth = type == GRAY ? 1 : 3;
		this.type = type;
		this.widDep = width * this.depth;
		if (pixels == null)
			this.raster = new float[width * height * this.depth];
		else {
			if (pixels.length != getSize())
				throw new IllegalArgumentException("The size of the pixel array" + pixels.length + " do not corresponds to the size of this kind of image: " + getSize());
			this.raster = pixels;
		}
	}

	public void set(int x, int y, int z, float value) {
		this.raster[y * this.widDep + x * this.depth + z] = value;
	}

	public void setSecure(int x, int y, int z, float value) {
		if (isOnRaster(x, y, z))
			this.raster[y * this.widDep + x * this.depth + z] = value;
	}

	public void clear() {
		Arrays.fill(this.raster, 0);
	}

	public void decrement(int x, int y, int z, float dec) {
		this.raster[y * this.widDep + x * this.depth + z] -= dec;
	}

	public float get(int x, int y, int z) {
		return this.raster[y * this.widDep + x * this.depth + z];
	}

	@Override
	public float[] getData() {
		return this.raster;
	}

	@Override
	public int getDepth() {
		return this.depth;
	}

	@Override
	public int getWidth() {
		return this.width;
	}

	@Override
	public int getHeight() {
		return this.height;
	}

	@Override
	public int getType() {
		return this.type;
	}

	@Override
	public String getStringType() {
		if (this.type == GRAY)
			return "GRAY";
		else if (this.type == RGB)
			return "RGBFLOAT";
		else if (this.type == BGR)
			return "BGRFLOAT";
		else if (this.type == YUV)
			return "YCBCRFLOAT";
		return "UNKNOW";
	}

	public float getIntValue(int x, int y, int z) {
		return this.raster[y * this.widDep + x * this.depth + z];
	}

	public float getIntValueSecure(int x, int y, int z) {
		if (x < 0 || x >= this.width || y < 0 || y >= this.height)
			throw new IllegalArgumentException("out of bound");
		return this.raster[y * this.widDep + x * this.depth + z];
	}

	public int getSize() {
		return this.widDep * this.height;
	}

	public void increment(int x, int y, int z, float inc) {
		this.raster[y * this.widDep + x * this.depth + z] += inc;
	}

	@Override
	public boolean isOnRaster(int x, int y) {
		return x >= 0 && x < this.width && y >= 0 && y < this.height;
	}

	@Override
	public boolean isOnRaster(int x, int y, int z) {
		return x >= 0 && x < this.width && y >= 0 && y < this.height && z >= 0 && z < this.depth;
	}

	public boolean isSameTypeAndSize(FloatRaster r) {
		return r != null && this.width == r.getWidth() && this.height == r.getHeight() && this.type == r.getType();
	}

	@Override
	public FloatRaster clone() {
		FloatRaster r = new FloatRaster(this.width, this.height, this.type);
		System.arraycopy(this.raster, 0, r.raster, 0, this.raster.length);
		return r;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ":" + this.width + "*" + this.height + "*" + this.depth + "_Hash:" + hashCode();
	}

	@Override
	public int getNbChannel() {
		return getDepth();
	}
}
