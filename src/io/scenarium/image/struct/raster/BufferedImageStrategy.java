/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.struct.raster;

import java.awt.image.BufferedImage;

import io.scenarium.core.struct.BufferedStrategy;

import javafx.geometry.Dimension2D;

public class BufferedImageStrategy extends BufferedStrategy<BufferedImage> {
	public BufferedImageStrategy(boolean isPageFlipping) {
		super(isPageFlipping);
	}

	// public void updateRasterSize() {
	// if (!isPageFlipping)
	// return;
	// if (e1.getWidth() != e2.getWidth() || e2.getHeight() != e1.getHeight() || e2.getType() != e1.getType())
	// e1 = new BufferedImage(e2.getWidth(), e2.getHeight(), e2.getType());
	// }

	@Override
	protected BufferedImage clone(BufferedImage bi) {
		return new BufferedImage(bi.getWidth(), bi.getHeight(), bi.getType());
	}

	public Dimension2D getDimension() {
		BufferedImage raster = getElement();
		return new Dimension2D(raster.getWidth(), raster.getHeight());
	}

	public int getType() {
		return getElement().getType();
	}
}
