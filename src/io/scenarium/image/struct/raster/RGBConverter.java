/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.struct.raster;

import java.awt.image.BufferedImage;

public interface RGBConverter {
	public Object getRaster(BufferedImage img);

	public boolean needToChangeType();

	public void toRGB(byte[] imgData);
}
