package io.scenarium.image.struct;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;

import io.beanmanager.editors.DynamicPossibilities;

/** @brief Compressed image is an aggregation of type of the image and the raw data */
public class CompressedImage implements Cloneable {
	public static final long serialVersionUID = 3L;
	// ! Type of the image format
	@DynamicPossibilities(possibleChoicesMethod = "getFormatAvaillableList")
	private final String format;
	// ! Raw data of the image compressed
	private final ByteBuffer dataRaw;
	private final long timestamp;

	public CompressedImage(String format, byte[] dataRaw) {
		this.format = format;
		this.dataRaw = ByteBuffer.wrap(dataRaw);
		this.timestamp = System.currentTimeMillis();
	}
	public CompressedImage(String format, byte[] dataRaw, long timestamp) {
		this.format = format;
		this.dataRaw = ByteBuffer.wrap(dataRaw);
		this.timestamp = timestamp;
	}

	public CompressedImage(String format, ByteBuffer dataRaw) {
		this.format = format;
		this.dataRaw = dataRaw;
		this.timestamp = System.currentTimeMillis();
	}
	public CompressedImage(String format, ByteBuffer dataRaw, long timestamp) {
		this.format = format;
		this.dataRaw = dataRaw;
		this.timestamp = timestamp;
	}

	public CompressedImage() {
		this.dataRaw = null;
		this.format = "";
		this.timestamp = System.currentTimeMillis();
	}

	public long getTimestamp() {
		return timestamp;
	}
	public String getFormat() {
		return this.format;
	}

	public byte[] getDataRaw() {
		if (this.dataRaw == null)
			return new byte[0];
		return this.dataRaw.array();
	}

	public ByteBuffer getData() {
		return this.dataRaw;
	}

	@Override
	public CompressedImage clone() {
		return new CompressedImage(this.format, this.dataRaw, this.timestamp);
	}

	public String[] getFormatAvaillableList() {
		ArrayList<String> writterformats = new ArrayList<>();
		HashSet<Class<?>> set = new HashSet<>();
		for (String writterFormatName : ImageIO.getWriterFormatNames()) {
			Iterator<ImageWriter> it = ImageIO.getImageWritersByFormatName(writterFormatName);
			while (it.hasNext()) {
				Class<?> writterClass = it.next().getClass();
				if (!set.contains(writterClass)) {
					set.add(writterClass);
					writterformats.add(writterFormatName.toUpperCase());
				}
			}
		}
		return writterformats.toArray(new String[writterformats.size()]);
	}

}
