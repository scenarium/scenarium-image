/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.image.struct;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;

import javax.vecmath.Matrix3d;

import io.scenarium.image.struct.raster.RGBConverter;

public class YUVColorSpace extends ColorSpace implements RGBConverter {
	private static final long serialVersionUID = 1L;
	private static ColorSpace csrgb = ColorSpace.getInstance(ColorSpace.CS_sRGB);
	private static Matrix3d yuvMat;
	static {
		yuvMat = new Matrix3d(new double[] { 65.738, 129.057, 25.064, -37.945, -74.494, 112.439, 112.439, -94.154, -18.285 });
		yuvMat.mul(1 / 256.0);
		yuvMat.invert();
	}

	public YUVColorSpace() {
		super(ColorSpace.TYPE_YCbCr, 3);
	}

	@Override
	public float[] fromCIEXYZ(float[] colorvalue) {
		float[] yuv = fromRGB(csrgb.fromCIEXYZ(colorvalue));
		yuv[1] = yuv[1] + 0.5f;
		yuv[2] = yuv[2] + 0.5f;
		return yuv;
	}

	@Override
	public float[] fromRGB(float[] rgbvalue) {
		float[] yuv = new float[3];
		yuv[0] = (float) (0.299 * rgbvalue[0] + 0.587 * rgbvalue[1] + 0.114 * rgbvalue[2]);
		yuv[1] = (float) (0.492 * (rgbvalue[2] - yuv[0]));
		yuv[2] = (float) (0.877 * (rgbvalue[0] - yuv[0]));
		return yuv;
	}

	@Override
	public Object getRaster(BufferedImage img) {
		return null;
	}

	@Override
	public boolean needToChangeType() {
		return false;
	}

	@Override
	public float[] toCIEXYZ(float[] colorvalue) {
		colorvalue[1] = colorvalue[1] - 0.5f;
		colorvalue[2] = colorvalue[2] - 0.5f;
		return csrgb.toCIEXYZ(toRGB(colorvalue));
	}

	@Override
	public void toRGB(byte[] imgData) {
		for (int i = 0; i < imgData.length; i += 3) {
			double y = (imgData[i] & 0xFF) - 16;
			int cb = (imgData[i + 1] & 0xFF) - 128;
			int cr = (imgData[i + 2] & 0xFF) - 128;
			double red = yuvMat.m00 * y + yuvMat.m01 * cb + yuvMat.m02 * cr;
			if (red < 0)
				red = 0;
			else if (red > 255)
				red = 255;
			imgData[i] = (byte) red;
			double green = yuvMat.m10 * y + yuvMat.m11 * cb + yuvMat.m12 * cr;
			if (green < 0)
				green = 0;
			else if (green > 255)
				green = 255;
			imgData[i + 1] = (byte) green;
			double blue = yuvMat.m20 * y + yuvMat.m21 * cb + yuvMat.m22 * cr;
			if (blue < 0)
				blue = 0;
			else if (blue > 255)
				blue = 255;
			imgData[i + 2] = (byte) blue;
		}
	}

	@Override
	public float[] toRGB(float[] yuv) {
		float[] rgb = new float[3];
		rgb[0] = yuv[0] + 1.13983f * yuv[2];
		rgb[1] = yuv[0] - 0.39465f * yuv[1] - 0.58060f * yuv[2];
		rgb[2] = yuv[0] + 2.03211f * yuv[1];
		return rgb;
		// return new float[]{yuv[0] + 1.13983f*yuv[2], yuv[0] - 0.39465f*yuv[1] - 0.58060f*yuv[2], yuv[0] + 2.03211f*yuv[1]};
	}
}
